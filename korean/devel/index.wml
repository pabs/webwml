#use wml::debian::template title="데비안 개발자 코너" MAINPAGE="true"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b" maintainer="Sebul"
#use wml::debian::recent_list
# Translator: Seunghun Han (kkamagui), 2021
# Transtator: Sebul, 2022

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>이 페이지의 정보와 다른 페이지를 향한 링크는 공개되어 있지만, 
이 사이트는 주로 데비안 개발자가 관심갖는 내용입니다.</p>
</aside>

<ul class="toc">
<li><a href="#basic">기본</a></li>
<li><a href="#packaging">패키징</a></li>
<li><a href="#workinprogress">진행중인 작업</a></li>
<li><a href="#projects">프로젝트</a></li>
<li><a href="#miscellaneous">기타</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">일반 정보</a></h2>
      <p>현재 개발자와 메인테이너 목록, 프로젝트에 참여하는 방법, 개발자 데이터베이스, 데비안 헌법, 투표 프로세스, 릴리스 및 아키텍처에 대한 링크.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">데비안 조직</a></dt>
        <dd>천 명 넘는 자원 봉사자들이 데비안 프로젝트에 참여합니다. 
이 페이지는 데비안 조직 구조를 설명하고, 팀들과 그 구성원, 그리고 연락처 주소를 나열합니다.</dd>
        <dt><a href="$(HOME)/intro/people">데비안 사람들</a></dt>
        <dd><a href="https://wiki.debian.org/DebianDeveloper">데비안 개발자 (DD)</a> (데비안 프로젝트의 정회원) 및 
 <a href="https://wiki.debian.org/DebianMaintainer">데비안 메인테이너 (DM)</a>가 프로젝트에 기여합니다. 
 <a href="https://nm.debian.org/public/people/dd_all/">데비안 개발자 목록</a> 및
 <a href="https://nm.debian.org/public/people/dm_all/">데비안 메인테이너 목록</a>을 보고 
 관계자에 대한 자세한 정보와 그들이 유지하는 패키지 정보를 찾으세요. 
<a href="developers.loc">데비안 개발자 세계지도</a> 및 
다양한 데비안 이벤트의 <a href="https://gallery.debconf.org/">갤러리</a>도 있습니다.</dd>
        <dt><a href="join/">데비안에 참여하기</a></dt>
        <dd>데비안 프로젝트는 자원봉사자로 구성되며, 
우리는 전반적으로 기술적 지식과 자유 소프트웨어에 대한 관심, 여유시간이 있는 새 개발자를 찾습니다.
여러분도 데비안을 도울 수 있습니다. 자세한 내용은 위의 관련 페이지를 보세요.</dd>
        <dt><a href="https://db.debian.org/">개발자 데이터베이스</a></dt>
        <dd>모두가 접근 가능한 기본 데이터가 있으며, 어떤 정보는 로그인 한 개발자들만 볼 수 있습니다.
이 데이터베이스에는 <a href="https://db.debian.org/machines.cgi">project machines</a> 및 
<a href="extract_key">개발자 GnuPG 키</a> 같은 정보가 있습니다. 
계정있는 개발자는 데비안 계정에 대해 <a href="https://db.debian.org/password.html">암호 바꾸기</a> 및 
<a href="https://db.debian.org/forward.html">메일 포워딩</a>을 어떻게 설정하는지 배울 수 있습니다. 
데비안 머신을 사용할 계획이라면, 반드시 <a href="dmup">데비안 머신 사용 정책</a>을 읽으세요.</dd>
        <dt><a href="constitution">데비안 헌법</a></dt>
        <dd>이 문서는 프로젝트에서 공식적 의사 결정을 위한 조직 구조를 설명합니다.</dd>
        <dt><a href="$(HOME)/vote/">투표 정보</a></dt>
        <dd>리더를 어떻게 뽑는지, 로고를 어떻게 고르고 보통 우리가 어떻게 투표 하는지에 대한 정보.</dd>
        <dt><a href="$(HOME)/releases/">릴리스</a></dt>
        <dd>이 페이지에는 현재 릴리스(<a href="$(HOME)/releases/stable/">stable</a>, 
<a href="$(HOME)/releases/testing/">testing</a> 및 
<a href="$(HOME)/releases/unstable/">unstable</a>) 및 
옛 릴리스 그리고 코드명이 있습니다.</dd>
        <dt><a href="$(HOME)/ports/">다른 아키텍처</a></dt>
        <dd>데비안은 많은 다른 아키텍처에서 돌아갑니다. 
이 페이지는 다양한 데비안 포트에 대한 정보를 수집하는데, 일부는 리눅스 커널을, 
다른 일부는 FreeBSD, NetBSD 및 Hurd 커널을 기반으로 합니다.</dd>
     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">패키징</a></h2>
      <p>데비안 개발자를 위한 데비안 정책, 절차 및 기타 자료, 그리고 새 메인테이너 안내서와 관련된 정책 매뉴얼 
및 기타 문서에 대한 링크.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">데비안 정책 매뉴얼</a></dt>
        <dd>이 매뉴얼은 데비안 배포에 대한 정책 요구 사항을 설명합니다. 
여기에는 데비안 아카이브의 구조와 내용, 운영 체제의 여러 설계 이슈, 배포판에 들어가기 위해 
각 패키지가 충족해야 하는 기술적 요구사항 등이 있습니다.
        <p>요컨대, 그걸 읽을 <strong>필요가 있습니다</strong>.</p>
        </dd>
      </dl>

      <p>정책과 관련된 다른 문서 중 관심이 있을 수 있는 문서가 몇 개 있습니다.</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">파일 시스템 계층 표준</a> (FHS)
        <br />FHS는 디렉터리 구조와 디렉터리 내용(파일 위치)을 정의하며, 
버전 3.0을 준수하는 것은 필수 사항입니다.
(데비안 정책 매뉴얼에서 <a 
href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">
9장</a>을 보세요).</li>
        <li><a href="$(DOC)/packaging-manuals/build-essential">build-essential 패키지</a> 목록
        <br />소프트웨어를 컴파일하거나 패키지 또는 패키지 세트를 작성하려면 
이러한 패키지를 가지고 있어야 합니다.
패키지 간의 관계를 선언할 때 <code>Build-Depends</code> 줄에 포함할 필요는 없습니다.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">메뉴 시스템</a>
        <br />데비안 메뉴 항목 구조.
        <a href="$(DOC)/packaging-manuals/menu.html/">메뉴 시스템</a> 문서도 보세요.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs 정책</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java 정책</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl 정책</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python 정책</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf Specification</a></li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">데이터베이스 응용프로그램 정책</a> (초안)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk 정책</a> (초안)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Ada에 대한 데비안 정책</a></li>
      </ul>

      <p><a href="https://bugs.debian.org/debian-policy">데비안 정책에 
변경된 제안</a>도 보세요.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">개발자 레퍼런스</a></dt>
        <dd>
데비안 개발자를 위한 권장 절차와 가용 리소스에 대한 개요 -- 또 다른 <strong>필독</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">데비안 메인테이너 가이드</a></dt>

        <dd>
많은 예제를 포함하여 데비안 패키지(공통 언어)를 빌드하는 방법. 
만약 당신이 데비안 개발자나 메인테이너가 될 계획이라면, 이것은 좋은 출발점입니다.
        </dd>
      </dl>
    </div>
  </div>
</div>

<h2><a id="workinprogress">진행 중인 작업: 활동하는 데비안 개발자 및 메인테이너 링크</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">데비안 &lsquo;Testing&rsquo;</a></dt>
  <dd>
&lsquo;unstable&rsquo; 배포판에서 자동으로 생성됨: 
여기에서 다음 데비안 릴리스를 고려하려면 패키지를 가져와야 합니다.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">릴리스 심각 버그</a></dt>
  <dd>이는 &lsquo;testing&rsquo; 배포판에서 패키지가 제거될 수도 있거나,
        어떤 경우에는 릴리스가 지연되기도 하는 버그 목록입니다.
        목록에서 &lsquo;심각&rsquo; 이상의 심각도를 가진 버그 리포트는
        가능한 한 빨리 패키지의 버그를 수정하세요.
  </dd>

  <dt><a href="$(HOME)/Bugs/">데비안 버그 추적 시스템 (BTS)</a></dt>
    <dd>버그를 보고하고, 토론하고, 수정하는 곳입니다. 
버그 추적 시스템은 데비안과 관련된 대부분의 문제 보고를 환영합니다.
BTS는 사용자와 개발자 모두에게 쓸모가 있습니다.
    </dd>
        
  <dt>데비안 패키지 정보</dt>
    <dd><a href="https://qa.debian.org/developer.php">패키지 정보</a> 및 
<a href="https://tracker.debian.org/">패키지 트래커</a> 웹 페이지는 
메인테이너에게 중요한 정보 모음을 제공합니다. 
다른 패키지를 추적하고 싶은 개발자는 (이메일을 통해) BTS 메일 사본과 업로드 및 설치 알림 메시지를 발송하는 서비스에 가입할 수 있습니다. 
자세한 내용은 <a
 href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">패키지 트래커 설명서</a>를 
참조하십시오.      
    </dd>

    <dt><a href="wnpp/">도움 필요한 패키지</a></dt>
      <dd>
작업이 필요하고 잠재적인 패키지(Work-Needing and Prospective Packages,
        WNPP)는 새로운 메인테이너가 필요한 데비안 패키지의 목록이며, 또한
        데비안에 아직 포함되지 않은 패키지의 목록이기도 합니다.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">유입(Incoming) 시스템</a></dt>
      <dd>
내부 아카이브 서버: 새 패키지가 업로드되는 위치입니다. 
허용된 패키지는 웹 브라우저를 통해 거의 즉시 이용할 수 있으며 
하루에 4번 <a href="$(HOME)/mirror/">미러</a>에 전파된다.
<strong>주의:</strong> &lsquo;incoming&rsquo;의 특성상 미러링하는 것은 안 좋습니다.
      </dd>

    <dt><a href="https://lintian.debian.org/">Lintian Reports</a></dt>
      <dd>
<a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>은 
패키지가 정책에 부합하는지 확인하는 프로그램입니다.
개발자는 매 업로드 전에 그것을 사용해야 합니다.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">데비안 &lsquo;Experimental&rsquo;</a></dt>
      <dd>
      &lsquo;experimental&rsquo; 배포판은 고도로 실험적인 소프트웨어의 임시 대기지역으로 쓰입니다. 
            <a href="https://packages.debian.org/experimental/">experimental
      패키지</a>는 &lsquo;unstable&rsquo;을 어떻게 쓰는지 알 때만 설치하세요.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">데비안 위키</a></dt>
      <dd>
개발자 및 기타 기여자를 위한 조언이 있는 데비안 위키.
      </dd>
</dl>

<h2><a id="projects">프로젝트: 내부 그룹 및 프로젝트</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">데비안 웹 페이지</a></li>
<li><a href="https://ftp-master.debian.org/">데비안 아카이브</a></li>
<li><a href="$(DOC)/ddp">데비안 문서 프로젝트 (DDP)</a></li>
<li><a href="https://qa.debian.org/">데비안 품질보증(QA) 팀</a></li>
<li><a href="$(HOME)/CD/">CD/DVD 이미지</a></li>
<li><a href="https://wiki.debian.org/Keysigning">키 사이닝</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">키 사이닝 협력</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">데비안 IPv6 프로젝트</a></li>
<li><a href="buildd/">Autobuilder Network</a> 및 그 <a href="https://buildd.debian.org/">빌드 로그</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">데비안 설명 번역 프로젝트 (DDTP)</a></li>
<li><a href="debian-installer/">데비안 설치관리자</a></li>
<li><a href="debian-live/">데비안 라이브</a></li>
<li><a href="$(HOME)/women/">데비안 여성</a></li>
<li><a href="$(HOME)/blends/">데비안 퓨어 블렌드</a></li>

</ul>

<h2><a id="miscellaneous">기타 링크</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li>컨퍼런스 발표 <a href="https://debconf-video-team.pages.debian.net/videoplayer/">비디오</a></li>
<li><a href="passwordlessssh">SSH 설정</a>으로 암호 묻지 않게 하기</li>
<li><a href="$(HOME)/MailingLists/HOWTO_start_list">메일링 리스트 요청</a>하기</li>
<li><a href="$(HOME)/mirror/">데비안 미러링</a> 정보</li>
<li><a href="https://qa.debian.org/data/bts/graphs/all.png">모든 버그 그래프</a></li>
<li>데비안에 들어가기를 기다리는 <a href="https://ftp-master.debian.org/new.html">새 패키지</a> (NEW Queue)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">새 데비안 패키지</a> 최근 7일</li>
<li><a href="https://ftp-master.debian.org/removals.txt">데비안에서 없어진 패키지</a></li>
</ul>
