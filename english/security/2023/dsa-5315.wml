<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>XStream serializes Java objects to XML and back again. Versions prior to
1.4.15-3+deb11u2 may allow a remote attacker to terminate the application with
a stack overflow error, resulting in a denial of service only via manipulation
of the processed input stream. The attack uses the hash code implementation for
collections and maps to force recursive hash calculation causing a stack
overflow. This update handles the stack overflow and raises an
InputManipulationException instead.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.4.15-3+deb11u2.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5315.data"
# $Id: $
