#use wml::debian::template title="Platform for Martin Michlmayr" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<h2>Introduction</h2>

<p>

I will start with a confession.  For a few years now, I've been thinking
that it's time to retire from the Debian project.  This is partly
because my interests have changed over the years, but partly because...
Debian just doesn't seem all that exciting anymore.  The only reason I
haven't actually resigned is because Debian and the Debian community
plays such an important role in my life and I don't want to let go.

</p>

<p>

I believe my mixed feelings about Debian are not that unusual.  We've
seen a number of prominent contributors leave the project in recent
times out of frustration or sheer boredom and I think there are many who
stick around but who have essentially checked out mentally.

</p>

<p>

Seeing that there was no candidate for the Debian Project Leader (DPL)
position, which I see as a symptom of grave underlying problems, got me
thinking.  I &mdash; like everyone who chooses to contribute to Debian
&mdash; have two options: I can resign, run away in frustration, or I
can help to discuss, confront, tackle and solve these problems.

</p>

<h2>Debian today</h2>

<p>

Debian appears to be in a very peculiar situation right now.  About
10-15 years ago, Debian faced an existential crisis.  A number of
factors contributed to Debian becoming less relevant.  This includes the
introduction of Ubuntu (which offered a more polished desktop at that
time and long term support for servers), the move towards Apple's macOS
(among the general public and even among open source developers,
especially in the web space), and a general shift towards mobile
devices.

</p>

<p>

Today, things are vastly different however.  Debian is more popular than
ever on servers.  There is a renewed understanding of the importance of
Debian and a new wave of adoption in the cloud and container space where
a stable operating system is key.  Debian is everywhere.  It runs large
parts of our society's IT infrastructure and also constitutes the
building blocks for many other solutions.  Users and corporations
appreciate not just the quality of the Debian system, but also the
maturity of our community and the open nature of our development process
which is not dominated by a single vendor.

</p>

<p>

So, despite this success and importance, why does it sometimes feel that
Debian has become somewhat irrelevant?  Why is there no buzz around
Debian that reflects Debian's success in so many areas?

</p>

<h2>Problems and solutions</h2>

<p>

I recently read Michael Stapelberg's post on <a href =
"https://michael.stapelberg.ch/posts/2019-03-10-debian-winding-down/">winding
down his Debian involvement</a> in which he describes how stagnant
Debian's infrastructure and processes have become.  His well-reasoned
post resonates with me, although I believe the problems are even bigger
than that.

</p>

<p>

The open source world has fundamentally changed in the last 5-10 years
in many ways.  Yet, if you look at Debian, we mostly operate the same
way we did 20 years ago.  Debian used to be a pioneer, a true leader.
Package managers, automatic upgrades, and packages builds on 10+
architectures &mdash; they were all novel, true innovations at the time.
The only significant innovation I can think of that came out of Debian
in recent years are reproducible builds.  Reproducible builds solve an
important problem and the idea has spread beyond Debian to the whole
FOSS world.

</p>

<p>

I hate when large companies talk about being "nimble" or similar
business buzz words.  But looking at Debian, I finally understand what
they mean &mdash; the project has evolved in a way that makes change
difficult.  We have failed to adopt to the new environment we find
ourselves in and we're struggling to keep up with an ever-faster
changing world.

</p>

<h3>Sustainability</h3>

<p>

When I got involved in free software in the 1990s, it was out of
technical curiosity and a passion for software freedom.  It was a hobby
and I never imagined making a career out of it.  But then more and more
companies discovered the technical superiority of software developed
with collaborative FOSS models coupled with the practical freedoms
offered by FOSS and the industry took off.  Many of us are nowadays
fortunate to contribute to FOSS not just in our spare time but as part
of our jobs.

</p>

<p>

Today, the majority of successful FOSS projects rely on (and thrive
from!) paid contributors.  This is not a failure, but a reflection of
the success of FOSS.  Unfortunately, compared to other successful
projects, the proportion of contributors who are paid is much smaller in
Debian and I see that as a failure.

</p>

<p>

Over the years, I have seen many valuable Debian contributors leave
after they graduate from university and get a job (often working on
FOSS, just not Debian; or working <em>with</em> Debian, but not
<em>on</em> Debian).  I have seen many dedicated contributors who
essentially have two jobs &mdash; a paid job during the day, and an
unpaid gig at Debian working long evenings and weekends.  I have met
many contributors who make huge sacrifices in order to contribute to
Debian.  Unfortunately, I have seen a lot of burnout, too (and
experienced it myself)!

</p>

<p>

If you want to work on Debian as a hobby, great!  But if you want to
make Debian your career, you should be able to do so.  While there are
some paid opportunities around Debian, I believe they are currently too
scarce and that we can take a number of actions so more contributors can
make Debian their careers.

</p>

<p>

First, we can help more companies get involved in Debian.  This does not
mean that we will sacrifice our neutrality or focus on quality.  It
means that we'll bring more contributors on board who will work towards
improving Debian for everyone.  We already have a number of companies
(Arm, credativ, Google, just to name a few) who make strategic
investments in Debian and pay engineers to work on Debian.

</p>

<p>

We can encourage more companies to get involved.  We can make it easier
for them to contribute, we can show companies that rely on Debian the
importance of actively getting engaged (and help them develop the
business case for it), and we can provide more incentives, such as
acknowledging their contributions better.

</p>

<p>

Second, grants are becoming a popular way to fund R&amp;D projects and
FOSS development.  The Reproducible Builds project is funded through
grants from the Linux Foundation and other organisations.  I doubt they
would have achieved as much as they did in relatively little time
without this funding.  We have a lot of smart people in Debian with good
ideas &mdash; the DPL can identify grants and help them apply for
funding.

</p>

<p>

Third, we need to work with the wider FOSS community to find more
sustainable ways to fund FOSS development.  The OpenSSL debacle has
shown that society depends on FOSS without making the necessary
investments.  Many independent FOSS developers struggle to make a living
from their work.  Platforms like Patreon exist but few can live from
this since there's not enough awareness that people need to invest in
and sponsor FOSS.

</p>

<p>

I'm sure there are other ways.  Overall, I believe it will become
increasingly difficult for significant projects with unpaid contributors
to compete with efforts where everyone is paid full-time and I think
that partly explains why Debian has struggled to keep up.  More
fundamentally, we also have to ask ourselves what kind of project we
want to be.  The world around us has changed and it's time to
acknowledge that.

</p>

<p>

The Long Term Support effort organised by Raphaël Hertzog of Freexian
has been spectacularly successfully.  Reproducible Builds has attracted
significant funding to improve an important aspect of Debian.  Is it
time to bring these efforts in and do them as Debian?  Or should we
encourage more such efforts around Debian and give them a helping hand?
Is it time to use our funds for certain things?

</p>

<p>

These are big questions we'll have to answer as a project eventually.
However, we can start by inviting more companies to participate in our
community and by encouraging those who depend on Debian to contribute.

</p>

<h3>Leadership and culture</h3>

<p>

I see a lack of leadership in Debian.  I don't only mean DPL-style
leadership, but lack of leadership in general.  The beauty of FOSS is
that anyone can make a contribution.  You don't need to get permission
to be a leader.  If you have a great idea, just do it!

</p>

<p>

Unfortunately, while we have many talented people in Debian, I think
we've reached a point where people are afraid to make or propose
changes, especially big, far-reaching changes.  They are afraid of the
resulting flame-war or other fallout (or simply tired of the cat herding
involved in getting the change adopted everywhere).

</p>

<p>

Why is it that our community makes people so afraid to speak?  Why have
we stalled?  It seems to me that Debian has developed a number of toxic
anti-patterns over the years that we have to move away from.

</p>

<p>

What I'm going to say now will be very controversial.  Debian prides
itself on being the universal operating system.  This is a great goal,
on many levels.  However, it's also important to acknowledge that you
cannot do everything &mdash; that doing everything slows things down.
We can't always wait for everyone, otherwise we'd never get things done.
Not everything is equally as important.  Sometimes it's important to set
priorities and to tell people "no".  Again, we're afraid to do that.
The universal operating system, and the underlying culture, while
laudable in practice, has become toxic in practice on some levels.

</p>

<p>

Same with technical excellence, another laudable goal, that becomes
toxic when taken to the extreme.  We talk and talk... and talk.  We
should remember that not every battle is worth fighting for and that not
every argument needs to be discussed till the bitter end.  It's fine to
agree to disagree sometimes.  We have to remind ourselves to pause,
reflect and ask: is it really worth it?  Do I really have to send this
email?  What about the social cost?  Let's stop draining energy from our
community!  We're not a debate club.  We're here to solve problems in a
practical manner and to ship solutions to users.

</p>

<p>

In addition to more leaders, we need more cheerleaders.  We have to
thank people.  We have to appreciate contributions.  We have to create a
buzz around Debian.  We should celebrate, and build upon, our success.
(And yes, I appreciate the irony of saying this in a platform which
essentially consists of a critique of Debian.  However, as DPL and as a
community member in general, I aim to say "thank you" more often and to
encourage other people to do the same.)

</p>

<h3>Influence</h3>

<p>

Debian plays a very special and important role in the FOSS ecosystem.
We are respected and our contributions are appreciated.  Debian
contributors tend to be leaders in the FOSS space.  We pride ourselves
not only on packaging software from upstream but on maintaining good
relationships.  This often results in us getting involved upstream and
taking on leadership roles there.  You can also look at current and past
board members of the Open Source Initiative (OSI) and again you'll see
many Debian people.

</p>

<p>

While Debian people play important roles everywhere, they often don't
represent the Debian project.  We need to learn to develop and speak as
a single voice.  Overall, I believe we, as a project, need to be more
vocal and take a more active role in influencing the FOSS ecosystem.
Debian has an incredible reputation but we don't use our clout for
important change.

</p>

<p>

While we're fairly good at influencing technical aspects, we have little
sway in the business world of FOSS.  And let's be honest &mdash; open
source has become a big business.  We're missing out of important
opportunities because this is a game we don't play well.  We need to
take a seat at the table (and, yes, that's often an actual table or a
conference call, and not email).

</p>

<h2>Background</h2>

<p><img src="tbm.jpg" alt="Photo of Martin Michlmayr" align="right"></p>

<p>

Who am I and how do I see the role of the DPL?  I have been a Debian
Developer since 2000 and I worked on a number of areas over the years.
I worked as an Application Manager, helping new contributors join the
project, and also managed the New Member Front Desk.  I contributed to
various Quality Assurance efforts, filed bug reports and introduced the
Missing in Action (MIA) process.  I ported Debian to various ARM and
MIPS devices and wrote documentation for it.  I served as Debian Project
Leader from 2003-2005.  Most of my contributions to Debian recently have
been indirectly through Software in the Public Interest, one of Debian's
trusted organisations.

</p>

<p>

I worked for Hewlett Packard for nine years, facilitating and leading
various internal and external FOSS activities.  I served on the board of
the Open Source Initiative for six years and on the board of Software in
the Public Interest for close to five years.  I currently serve on the
board of Software Freedom Conservancy, the home of the Debian Copyright
Aggregation Project.  Over the years I've become an open source
accounting geek and I contribute to ledger, beancount, and
ledger2beancount.

</p>

<p>

There are various opinions on what kind of time commitment being DPL
takes, ranging from proposals to a "leaderless" Debian to the DPL being
a full-time job.  Based on my experience as DPL in the past, I'm firmly
in the latter camp.  When I was DPL in the past, I was a student and had
(what looking back seems like) unlimited time.  I'm now a FOSS
consultant and every hour I'd spend as DPL takes away from that work.  I
am, however, willing to devote significant time to Debian.  (I might
also set up a virtual tin cup.)

</p>

<p>

The role of DPL is very unique.  In Debian you don't typically get
"promoted" in the traditional sense.  You start doing the work and
eventually people recognise you for it.  It's all based on reputation
and ideally we want a DPL who has already earned the reputation for
being a leader.  I gave a talk about FOSS culture at an event some time
ago and I emphasised the importance of building up a good reputation.
To illustrate the point, I said there's this one particular person in
Debian and when this person sends an email, people will listen because
they know that the email will contain genuine wisdom written in a calm
tone.  Despite Debian having over a thousand <em>incredible</em>
contributors, this description was enough for some Debian Developers in
the audience to nod their heads with a shared understanding of whom I
was talking about (without ever mentioning the person's name).  This is
the kind of reputation you want to build up!  (And there are many other
contributors I could describe for their very unique ways of contributing
to Debian!)

</p>

<p>

Unfortunately, I haven't been active in Debian lately and so I don't
have the reputation that I would hope to see from a DPL.  While many
old-timers are familiar with my work, I'm a blank canvas for many new
contributors.  I am aware of this, and if elected, I will work hard to
earn their trust.

</p>

<h2>The DPL</h2>

<p>

The role of DPL is often described as having an internal and an external
function and both are absolutely vital.  I believe you can also define
three roles of the DPL: the DPL as administrator, facilitator, and
leader.

</p>

<p>

While I enjoy getting things done, I also find it immensely rewarding to
help other people get work done.  In this way, I see the DPL as a
facilitator.  I just attended FOSSASIA where someone compared managing
communities to conducting an orchestra.  When you look at orchestra
conductors, you may wonder what they are doing apart from waving their
hands around.  You don't see all the coordination and practice that was
necessary for a flawless performance.  DPLs have in the past been
criticised for not being active in part because you don't see a lot of
the day to day work.  You only see if something doesn't get done or goes
wrong!

</p>

<p>

Facilitation includes a lot of coordination.  It involves helping people
do their work effectively, removing problems, connecting people, using
delegation and following up.  It involves listening to people.  It also
involves asking people to do certain tasks.  You'd be surprised how
effective it can be to ask someone to help out.  If you don't ask, it
doesn't get done.  Facilitation doesn't have to be <em>reactive</em> as
it's often been.  I'd like to be pro-active in terms of resolving
problems and identifying areas that need to be tackled.

</p>

<p>

The external leadership role is also important.  There are discussions
about replacing the DPL with a board or committee, and while it's
important to have these conversations and to evolve our governance, I
think we should not discount various human factors.  Companies, the
press and others often want to talk to the leader of the project.  They
don't want to talk to a mailing list, or a committee and sometimes not
even to a delegate.  As DPL, I intend to work with delegates on forming
more connections and partnerships; on building bridges with our allies.
I'd also like to start an honest conversation about the state of Debian.
How can we improve our culture?  What are the things holding us back?

</p>

<p>

Finally, it's important to take a step back and look at the big picture.
This platform is highly critical of Debian in many ways.  I should say
that I'm a critical person by nature and I tend to emphasise the
negative side.  Fortunately, I've been able to use that personality flaw
in positive ways, such as through my QA work where I identify bugs and
that feedback is used to improve the software.  But it's important to
highlight the good aspects.  Debian is so unique, so wonderful and so
special in many ways.  If I didn't believe in Debian, I wouldn't run in
this DPL election.  I acted as DPL before and know how difficult being
the DPL can be sometimes.  Yes, I see severe problems in Debian, but I
firmly believe that together we can solve them!

</p>


<h2>Rebuttal</h2>

<h3>Joerg Jaspert</h3>

<p>

Joerg describes a number of important questions for Debian and the DPL.
I agree with his point that it's important to listen to users, including
former users who moved away from Debian.  While I generally agree with
the points described in his platform, I find it a bit "generic".  It's
not clear exactly what he hopes to accomplish or how he would be
different to the other candidates.  Then again, as Joerg points out,
every DPL has their own unique style; and at the end of the day, we
elect a person and not a platform.

</p>

<h3>Jonathan Carter</h3>

<p>

Jonathan raises a number of important points in his platform. I
definitely agree that it's important to improve community processes and
to foster our community, and that fixing communication is an important
step to improving a lot of problems in the project.  I also agree with
his view on the DPL as an enabler and someone who is approachable (in my
view, both within the project as well as to other parties in the
community).

</p>

<p>

Normally, I would agree that it's important to make small, incremental
changes, but I think Debian has reached a point where it's important to
fundamentally rethink how our community operates.

</p>

<h3>Sam Hartman</h3>

<p>

I like Sam's platform and, even though they are different in many ways,
I see a lot of parallels between our platforms.  He very eloquently
describes that, "success in a community is emotional not just technical"
and that Debian can sometimes be draining.  This is similar to the toxic
anti-patterns I describe that cause contributors to fight, mentally tune
out or resign.  What I don't see in Sam's platform are goals to make
Debian more relevant and more sustainable in today's environment.

</p>

