<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>More deserialization flaws were discovered in jackson-databind
relating to the classes in com.zaxxer.hikari.HikariConfig,
com.zaxxer.hikari.HikariDataSource, commons-dbcp and
com.p6spy.engine.spy.P6DataSource, which could allow an
unauthenticated user to perform remote code execution. The issue was
resolved by extending the blacklist and blocking more classes from
polymorphic deserialization.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.2-2+deb8u9.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1943.data"
# $Id: $
