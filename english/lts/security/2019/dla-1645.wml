<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues in wireshark, a network traffic analyzer, have been found.
Dissectors of:</p>
<ul>
<li>ISAKMP, a Internet Security Association and Key Management Protocol</li>
<li>P_MUL, a reliable multicast transfer protocol</li>
<li>6LoWPAN, IPv6 over Low power Wireless Personal Area Network</li>
</ul>
<p>are affected.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5719">CVE-2019-5719</a>

    <p>Mateusz Jurczyk found that a missing encryption block in a packet could
    crash the ISAKMP dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5717">CVE-2019-5717</a>

   <p>It was found that the P_MUL dissector could crash when a malformed
   packet contains an illegal Data PDU sequence number of 0.  Such a packet
   may not be analysed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5716">CVE-2019-5716</a>

   <p>It was found that the 6LoWPAN dissector could crash when a malformed
   packet does not contain IPHC information though the header says it
   should.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u17.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1645.data"
# $Id: $
