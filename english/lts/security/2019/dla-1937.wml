<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An open redirect, that allows an attacker to write an arbitrary file with
supplied filename and content to the current directory, by redirecting a
request from HTTP to a crafted URL pointing to a server in his or hers control,
was found and reported in <a href="https://security-tracker.debian.org/tracker/CVE-2019-10751">CVE-2019-10751</a>.
This was patched upstream and so when `--download` without `--output` results
in a redirect, now only the initial URL is considered, not the final one.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.8.0-1+deb8u1.</p>

<p>We recommend that you upgrade your httpie packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1937.data"
# $Id: $
