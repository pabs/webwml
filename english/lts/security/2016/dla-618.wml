<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via Plan 9
File System(9pfs) support, is vulnerable to a directory/path traversal issue.
It could occur while creating or accessing files on a shared host directory.</p>

<p>A privileged user inside guest could use this flaw to access undue files on the
host.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u15.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-618.data"
# $Id: $
