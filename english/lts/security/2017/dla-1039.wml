<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7480">CVE-2017-7480</a>

      <p>The original patch introduces new regex to better check for
      allowed download URLs.
      Other versions of the package in Jessie, Stretch and Sid don't
      apply that patch but just disable the download of everything by
      default via rkhunter.conf.
      In order to make this version consistent with all the other
      distributions and don't break existing installations, this will
      be done in Wheezy as well.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.0-1+deb7u1.</p>

<p>We recommend that you upgrade your rkhunter packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1039.data"
# $Id: $
