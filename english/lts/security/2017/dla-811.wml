<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been fixed in libplist:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5209">CVE-2017-5209</a>

    <p>Out of bounds read when parsing specially crafted Apple plist file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5545">CVE-2017-5545</a>

    <p>Heap buffer overflow via crafted Apple plist file</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8-1+deb7u1.</p>

<p>We recommend that you upgrade your libplist packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-811.data"
# $Id: $
