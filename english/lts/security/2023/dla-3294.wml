<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libarchive, a multi-format archive and
compression library.
Due to missing checks after calloc, null pointer dereferences might
happen.</p>


<p>For Debian 10 buster, this problem has been fixed in version
3.3.3-4+deb10u3.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>For the detailed security status of libarchive please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libarchive">https://security-tracker.debian.org/tracker/libarchive</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3294.data"
# $Id: $
