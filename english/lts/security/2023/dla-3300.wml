<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An issue was discovered in Glance, OpenStack Image Registry and Delivery
Service - Daemons. By supplying a specially created VMDK flat image that
references a specific backing file path, an authenticated user may
convince systems to return a copy of that file's contents from the
server, resulting in unauthorized access to potentially sensitive data.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:17.0.0-5+deb10u1.</p>

<p>We recommend that you upgrade your glance packages.</p>

<p>For the detailed security status of glance please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/glance">https://security-tracker.debian.org/tracker/glance</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3300.data"
# $Id: $
