<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of issues in graphite-web, a
tool provide realtime graphing of system statistics etc.</p>

<p>A series of cross-site scripting (XSS) vulnerabilties existed that could
have been exploited remotely. Issues existed in the Cookie Handler, Template
Name Handler and Absolute Time Range Handler components:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4728">CVE-2022-4728</a>

    <p>A vulnerability classified as problematic has been found in
    SourceCodester Blood Bank Management System 1.0. Affected is an unknown
    function of the file index.php?page=users of the component User
    Registration Handler. The manipulation of the argument Name leads to cross
    site scripting. It is possible to launch the attack remotely. VDB-216774 is
    the identifier assigned to this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4729">CVE-2022-4729</a>

    <p>A vulnerability classified as critical was found in SourceCodester
    School Dormitory Management System 1.0. Affected by this vulnerability is
    an unknown functionality of the component Admin Login. The manipulation
    leads to sql injection. The attack can be launched remotely. The associated
    identifier of this vulnerability is VDB-216775.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4730">CVE-2022-4730</a>

    <p>A vulnerability was found in Graphite Web. It has been classified as
    problematic. Affected is an unknown function of the component Absolute Time
    Range Handler. The manipulation leads to cross site scripting. It is
    possible to launch the attack remotely. The exploit has been disclosed to
    the public and may be used. The name of the patch is
    2f178f490e10efc03cd1d27c72f64ecab224eb23. It is recommended to apply a
    patch to fix this issue. The identifier of this vulnerability is
    VDB-216744.</li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1.1.4-3+deb10u2.</p>

<p>We recommend that you upgrade your graphite-web packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3309.data"
# $Id: $
