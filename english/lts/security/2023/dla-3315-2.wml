<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One of the security fixes released as DLA 3315 introduced a regression in the
processing of WAV files with variable bitrate encoding. Updated sox packages are
available to correct this issue.</p>

<p>For Debian 10 buster, this problem has been fixed in version
14.4.2+git20190427-1+deb10u2.</p>

<p>We recommend that you upgrade your sox packages.</p>

<p>For the detailed security status of sox please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sox">https://security-tracker.debian.org/tracker/sox</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3315-2.data"
# $Id: $
