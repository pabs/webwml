<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of vulnerabilies in the xrdp
Remote Desktop Protocol (RDP) server:</p>

<ul>

<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23480">CVE-2022-23480</a>:
Prevent a series of potential buffer overflow vulnerabilities in the
devredir_proc_client_devlist_announce_req() function.</li>

<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23481">CVE-2022-23481</a>:
Fix an out-of-bounds read vulnerability in the
xrdp_caps_process_confirm_active() function.</li>

<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23480">CVE-2022-23480</a>:
Fix an out-of-bounds read vulnerability in the
xrdp_sec_process_mcs_data_CS_CORE() function.</li>
</ul>
<p>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
0.9.9-1+deb10u3.</p>

<p>We recommend that you upgrade your xrdp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3375.data"
# $Id: $
