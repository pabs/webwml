<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was found in Ghostscript, the GPL PostScript/PDF
interpreter. It was discovered that some privileged Postscript operators
remained accessible from various places. For instance a specially crafted
PostScript file could use this flaw in order to have access to the file
system outside of the constrains imposed by -dSAFER.</p>

<p>This problem exists because of an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-3839">CVE-2019-3839</a>.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
9.26a~dfsg-0+deb9u9.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>For the detailed security status of ghostscript please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ghostscript">https://security-tracker.debian.org/tracker/ghostscript</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2989.data"
# $Id: $
