<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an arbitrary object deserialization
vulnerability in php-horde-turba, an address book component for the Horde
groupware suite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30287">CVE-2022-30287</a>

    <p>Horde Groupware Webmail Edition through 5.2.22 allows a reflection
    injection attack through which an attacker can instantiate a driver class.
    This then leads to arbitrary deserialization of PHP objects.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
4.2.23-1+deb10u1.</p>

<p>We recommend that you upgrade your php-horde-turba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3090.data"
# $Id: $
