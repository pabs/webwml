<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in Git, a fast, scalable,
distributed revision control system, which may affect multi-user systems.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21300">CVE-2021-21300</a>

    <p>A specially crafted repository that contains symbolic links as well as
    files using a clean/smudge filter such as Git LFS, may cause just-checked
    out script to be executed while cloning onto a case-insensitive file system
    such as NTFS, HFS+ or APFS (i.e. the default file systems on Windows and
    macOS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40330">CVE-2021-40330</a>

    <p>git_connect_git in connect.c allows a repository path to contain a newline
    character, which may result in unexpected cross-protocol requests, as
    demonstrated by the git://localhost:1234/%0d%0a%0d%0aGET%20/%20HTTP/1.1
    substring.</p>


<p>For Debian 10 buster, these problems have been fixed in version
1:2.20.1-2+deb10u4.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3145.data"
# $Id: $
