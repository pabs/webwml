<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was reported in src:elog, a logbook system to manage
notes through a Web interface. This vulnerability allows remote
attackers to create a denial-of-service condition on affected
installations of ELOG Electronic Logbook. Authentication is not
required to exploit this vulnerability. The specific flaw exists
within the processing of HTTP parameters. A crafted request can
trigger the dereference of a null pointer. An attacker can leverage
this vulnerability to create a denial-of-service condition.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.1.2-1-1+deb9u1.</p>

<p>We recommend that you upgrade your elog packages.</p>

<p>For the detailed security status of elog please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/elog">https://security-tracker.debian.org/tracker/elog</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3014.data"
# $Id: $
