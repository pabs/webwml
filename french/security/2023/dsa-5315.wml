#use wml::debian::translation-check translation="31dd56ffc3a925fcd62629a94767920c8bfbbfd1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>XStream sérialise des objets Java vers XML et inversement. Les versions
antérieures à 1.4.15-3+deb11u2 peuvent permettre à un attaquant distant de
mettre fin à l'application avec une erreur de dépassement de pile, menant à
un déni de service seulement au moyen de la manipulation du flux d'entrée
traité. Les attaques utilisent l'implémentation du code de hachage pour les
collections et les <q>maps</q> pour obliger à un calcul de hachage récursif
provoquant un dépassement de pile. Cette mise à jour s'occupe du
dépassement de pile et déclenche une InputManipulationException à la place.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 1.4.15-3+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5315.data"
# $Id: $
