#use wml::debian::translation-check translation="939d345dff81495662cf678797b91dc1fc31a14e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42826">CVE-2022-42826</a>

<p>Francisco Alonso a découvert que le traitement d'un contenu web
contrefait pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23517">CVE-2023-23517</a>

<p>YeongHyeon Choi, Hyeon Park, SeOk JEON, YoungSung Ahn, JunSeo Bae et
Dohyun Lee ont découvert que le traitement d'un contenu web contrefait
pouvait pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23518">CVE-2023-23518</a>

<p>YeongHyeon Choi, Hyeon Park, SeOk JEON, YoungSung Ahn, JunSeo Bae et
Dohyun Lee ont découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.38.4-2~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5340.data"
# $Id: $
