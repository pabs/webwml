#use wml::debian::translation-check translation="9ab93a9dd660a1a40a9efa07deabb811a5eb3cbc" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0108">CVE-2022-0108</a>

<p>Luan Herrera a découvert qu'un document HTML peut être capable de
fournir des cadres iframes avec des informations sensibles de
l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32885">CVE-2022-32885</a>

<p>P1umer et Q1IQ ont découvert que le traitement d'un contenu web
contrefait pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27932">CVE-2023-27932</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
contrefait pouvait contourner la politique de même origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27954">CVE-2023-27954</a>

<p>Un chercheur anonyme a découvert qu'un site web peut être capable de
pister des informations sensibles de l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28205">CVE-2023-28205</a>

<p>Clement Lecigne et Donncha O Cearbhaill ont découvert que le traitement
d'un contenu web contrefait pouvait conduire à l'exécution de code
arbitraire. Apple a eu connaissance d'un rapport indiquant que ce problème
peut avoir été activement exploité.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.40.1-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5396.data"
# $Id: $
