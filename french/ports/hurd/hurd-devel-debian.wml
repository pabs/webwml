#use wml::debian::template title="Debian GNU/Hurd&nbsp;&ndash;&nbsp;Développement" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="e42a3c19fa8c376678e6147f47b31ba3fc60e369" maintainer="Jean-Paul Guillonneau"

# Translators:
# Mohammed Adnène Trojette 2005-2007.
# David Prévot, 2010-2013.
# Jean-Paul Guillonneau, 2016-2020

<h1>
Debian&nbsp;GNU/Hurd</h1>
<h2>
Développement de la distribution</h2>

<h3>
Empaqueter des logiciels pour Hurd</h3>
<p>
Les paquets spécifiques à Hurd sont entretenus dans
<url "https://salsa.debian.org/hurd-team/">.

<h3>
Porter des paquets Debian</h3>
<p>
Si vous souhaitez aider le portage Debian&nbsp;GNU/Hurd, vous devriez
vous familiariser avec le système d'empaquetage de Debian. Une fois
que vous l'aurez fait en lisant la documentation disponible et en
visitant le <a href="$(HOME)/devel/">Coin du développeur</a>, vous devriez
savoir comment extraire les paquets source Debian et empaqueter
un paquet Debian. Voici un cours intensif pour les personnes très
paresseuses&nbsp;:</p>

<h3>
Obtenir le source et empaqueter des paquets</h3>
<p>
Le code source peut être obtenu en exécutant simplement
<code>apt source paquet</code>, ce qui extrait aussi les sources.
</p>
<p>
Extraire un paquet source Debian nécessite le fichier
<code>paquet_version.dsc</code> et les fichiers qui y sont listés.
Vous créez le répertoire d'empaquetage Debian avec la commande
<code>dpkg-source -x paquet_version.dsc</code>.
</p>

<p>
La construction du paquet se fait dans le nouveau répertoire
d'empaquetage Debian <code>paquet-version</code> avec
la commande <code>dpkg-buildpackage -B "-mMonNom &lt;MonAdresseÉlectronique&gt;"</code>.
Vous pouvez utiliser
<code>-b</code> au lieu de <code>-B</code> si vous voulez aussi compiler
les parties indépendantes de l'architecture du paquet (mais c'est généralement
inutile dans la mesure où elles sont disponible dans l'archive, et leur
construction peut nécessiter des dépendances supplémentaires).
Vous pouvez ajouter <code>-uc</code> pour éviter de signer le paquet avec votre
clef GPG.
</p>
<p>
La construction pourrait nécessiter d’installer des paquets supplémentaires.
Le plus simple est d’exécuter <code>apt build-dep paquet</code>
qui installera tous les paquets nécessaires.
</p>

<p>
Utiliser pbuilder peut être pratique. Il peut être construit avec
<code>sudo pbuilder create --mirror http://deb.debian.org/debian-ports/ --debootstrapopts --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg --debootstrapopts --extra-suites=unreleased --extrapackages debian-ports-archive-keyring</code>,
et <code>pdebuild -- --binary-arch</code> peut être utilisé et gérera le
téléchargement des dépendances de construction, etc, et mettra le résultat dans
<code>/var/cache/pbuilder/result</code>.
</p>

<h3>
Choisissez un paquet</h3>
<p>
Sur quels paquets faut-il travailler ? À vrai dire,
tous les paquets qui ne sont pas encore portés, mais qui nécessitent de l’être.
Cela change constamment, alors il est conseillé de se focaliser
d'abord sur les paquets ayant beaucoup de dépendances inverses,
qui sont visibles sur le graphique de dépendance des paquets
<url "https://people.debian.org/~sthibault/graph-radial.pdf">
mis à jour quotidiennement ou sur la liste des paquets les plus demandés
<url "https://people.debian.org/~sthibault/graph-total-top.txt"> (c'est
la liste des demandes à long terme, la liste des demandes à court
terme est <url "https://people.debian.org/~sthibault/graph-top.txt">).

C'est généralement une bonne idée aussi d'en prendre parmi la liste des
paquets périmés <url "https://people.debian.org/~sthibault/out_of_date2.txt"> et
<url "https://people.debian.org/~sthibault/out_of_date.txt">,
car ils ont fonctionné et qu'ils ne sont probablement cassés qu'à cause d'un
petit nombre de raisons.

Vous pouvez aussi en prendre un au hasard
parmi les paquets manquants, surveiller les journaux
des processus d'empaquetage automatique sur la liste de diffusion
debian-hurd-build-logs ou utiliser la liste de wanna-build en
<url "https://people.debian.org/~sthibault/failed_packages.txt">. Quelques
problèmes de construction sont plus faciles à résoudre que d’autres,
classiquement, «·undefined reference to foo·», ou foo consiste en quelque chose
comme pthread_create, dlopen, cos…, (qui sont bien évidemment présents dans
hurd-i386), qui montre que l’étape de configuration du paquet a aussi oublié
d’inclure -lpthread, -ldl, -lm, etc., sur le Hurd. Notez que les fonctions ALSA
MIDI ne sont pas disponibles.
</p>

<p>
Vérifiez également si le travail a déjà été fait sur
<url "https://alioth.debian.org/tracker/?atid=410472&amp;group_id=30628&amp;func=browse">,
<url "https://alioth.debian.org/tracker/?atid=411594&amp;group_id=30628&amp;func=browse">,
sur le BTS (<url "https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-hurd@lists.debian.org;tag=hurd">),
<url "https://wiki.debian.org/Debian_GNU/Hurd"> et l'état
en temps réel des paquets sur buildd.debian.org, par
exemple <url "https://buildd.debian.org/util-linux">.
</p>

<h4>
Paquets qui ne seront pas portés</h4>
<p>
Quelques paquets parmi ceux qui suivent, ou des parties de ces paquets,
seront peut-être portables plus tard, mais ils sont actuellement
au moins considérés comme non portables.
Ils sont normalement marqués <q>NotForUs</q> dans la base de données de buildd.
</p>

<ul>
<li>
<code>base/makedev</code>, parce que le Hurd apporte ses propres versions
de ce script. Le paquet source Debian ne contient qu'une version
spécifique à Linux.</li>
<li>
<code>base/modconf</code> et <code>base/modutils</code>, parce que
les modules sont un concept spécifique à Linux.</li>
<li>
<code>base/netbase</code>, parce que ce qui s'y trouve
est hautement spécifique au noyau Linux. Le Hurd utilise
<code>inetutils</code> à la place.</li>
<li>
<code>base/pcmcia-cs</code>, parce que ce paquet est spécifique à Linux.
</li>
<li>
<code>base/setserial</code>, parce que c'est spécifique au noyau Linux.
Cependant, avec le portage des pilotes de caractères Linux sur GNU Mach,
nous pourrons peut-être les utiliser.</li>
</ul>

<h3><a name="porting_issues">
Problèmes généraux de portage</a></h3>

<p>
  Une <a href=https://www.gnu.org/software/hurd/hurd/porting/guidelines.html>liste
  des problèmes courants</a> est disponible sur le site amont.

  Les problèmes courants suivants sont spécifiques à Debian.
</p>

<p>
Avant d'essayer de corriger quoi que ce soit, vérifiez si le
portage kfreebsd* n'a pas déjà préparé des correctifs, qui
demandent juste à être adaptés pour convenir aussi à hurd-i386.
</p>

<ul>
<li>
<code>foo : Depends: foo-data (= 1.2.3-1) but it is not going to be installed</code>
<p>
La réponse courte est : la construction du paquet <code>foo</code> a échoué sur
hurd-i386, et cela doit être corrigé, recherchez l'échec de construction sur sa
page d'état dans buildd.debian.org.
</p>
<p>
Cela arrive habituellement quand la construction du paquet <code>foo</code>
échoue actuellement, mais qu'il se construisait bien auparavant. Utilisez
<code>apt-cache policy foo foo-data</code> pour voir si par exemple la version
<code>1.2.3-1</code> de <code>foo</code> est disponible et si une version plus
récente de <code>foo-data</code> <code>2.0-1</code> est disponible. C'est parce
que dans les debian-ports, les paquets indépendants de l'architecture (arch:all)
sont partagés par toutes les architectures et donc, quand une version plus
récente du paquet source <code>foo</code> (qui construit mes paquets binaires
<code>foo</code> et <code>foo-data</code>) est introduite, le nouveau paquet
<code>foo-data</code> arch:all est installé, même si le nouveau paquet binaire
<code>foo</code> pour hurd-i386 ne peut pas être construit, menant ainsi à des
versions incompatibles. Pour corriger cela, il est nécessaire que la
construction de l'archive debian-ports utilise dak à la place de mini-dak, ce
qui est un travail qui n'est pas encore achevé.
</p>

</li>
<li>
<code>some symbols or patterns disappeared in the symbols file</code>
<p>
Certains paquets entretiennent une liste des symboles qui sont censés
apparaître dans les bibliothèques. Cette liste est cependant normalement
obtenue sur un système Linux et donc inclut des symboles qui peuvent ne pas
avoir de sens sur les systèmes autres que Linux (par exemple, du fait d'une
fonctionnalité propre à Linux). Il est néanmoins possible d'introduire des
conditions dans le fichier <code>.symbols</code>, par exemple :
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 (arch=linux-any)linuxish_function@Base 1.23
</pre></td></tr></table>

</li>

  <li>
    <code>Broken libc6 dependency</code>
    <p>
      Certains paquets utilisent une dépendance erronée à <code>libc6-dev</code>.

      C'est incorrect parce que <code>libc6</code> est
      spécifique à certaines architectures GNU/Linux.

      Le paquet GNU correspondant est <code>libc0.3-dev</code>, mais
      les autres systèmes d'exploitation en utilisent de différents.

      Le problème peut être localisé avec le fichier
      <code>debian/control</code> de l'arborescence source.

      Parmi les solutions typiques, il est possible de détecter
      le système d'exploitation avec <code>dpkg-architecture</code>
      et de mettre « en dur » (« hardcode ») le soname, ou mieux,
      utiliser un OU logique.

      Par exemple :
<code>libc6-dev | libc6.1-dev | libc0.3-dev | libc0.1-dev | libc-dev</code>.

      <code>Libc-dev</code> est un paquet virtuel qui
      fonctionne pour n'importe quel soname, mais il
      ne faut le placer qu'en dernière option.
    </p>
  </li>

<li>
<code>undefined reference to snd_*, SND_* undeclared</code>
<p>
Certains paquets utilisent ALSA même sur les architectures non Linux.

Le paquet oss-libsalsa fournit quelques émulations à l'aide d'OSS, mais
il est limité à la version 1.0.5 d'ALSA, et certaines fonctionnalités ne sont pas
fournies, comme par exemple toutes les opérations de séquenceur.
</p>
<p>
Si le paquet le permet, la prise en charge d'ALSA devrait être
désactivée pour les architectures <code>!linux-any</code> (par exemple
à l'aide d'une option de <code>configure</code>), un qualificatif
<code>[linux-any]</code> ajouté au <code>Build-Depends</code> d'ALSA,
et l'inverse ajouté à <code>Build-Conflicts</code>, comme par exemple
<code>Build-Conflicts: libasound2-dev [!linux-any]</code>.
</p>
</li>
<li>
<code>dh_install: Cannot find (any matches for) "foo" (tried in ., debian/tmp)</code>
<p>
Cela se produit habituellement lorsque l’amont n’installe pas quelque chose
parce qu’il ne reconnaît pas le système d’exploitation. Quelquefois c’est tout
bête (par exemple, il ne sait pas que construire une bibliothèque sur GNU/Hurd
se fait exactement comme sur GNU/Linux) et cela nécessite d’être corrigé.
Quelquefois cela est réellement sensé (par exemple, installation des fichiers
de service de systemd). Dans ce cas, il est possible d’utiliser dh-exec : la
construction dépend de <tt>dh-exec</tt>, <tt>chmod +x</tt> du fichier
<tt>.install</tt>, et de préfixer les lignes problématiques avec, par exemple,
<tt>[linux-any]</tt> ou <tt>[!hurd-any]</tt>.
</p>
</li>
</ul>

<h3> <a name="debian_installer">
Modifier l'installateur Debian</a></h3>

<p>
Pour construire une image ISO, le plus simple est de partir d'une image
existante issue de <a href=hurd-cd>la page des images CD de Hurd</a>.
Vous pouvez alors la monter et la copier :
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
mount debian-sid-hurd-i386-NETINST-1.iso /mnt
cp -a /mnt /tmp/mon_image
umount /mnt
chmod -R +w /tmp/mon_image
</pre></td></tr></table>

<p>
Vous pouvez monter le disque virtuel de démarrage et par exemple remplacer un
traducteur par votre propre version :
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
gunzip /tmp/mon_image/initrd.gz
mount /tmp/mon_image/initrd /mnt
cp ~/hurd/rumpdisk/rumpdisk /mnt/hurd/
umount /mnt
gzip /tmp/mon_image/initrd
</pre></td></tr></table>

<p>
Vous pouvez reconstruire l'ISO avec grub-mkrescue :
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
rm -fr /tmp/mon_image/boot/grub/i386-pc
grub-mkrescue -o /tmp/mon_image.iso /tmp/mon_image
</pre></td></tr></table>

</li>
</ul>
