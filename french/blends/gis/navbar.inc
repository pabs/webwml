#use wml::debian::blends::gis

{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/gis/">Debian&nbsp;GIS&nbsp;Blend</a></p>
    <ul>
      <li><a href="$(HOME)/blends/gis/about">À&nbsp;propos&nbsp;du&nbsp;mélange</a></li>
      <li><a href="$(HOME)/blends/gis/contact">Contact</a></li>
      <li><a href="$(HOME)/blends/gis/get/">Obtention&nbsp;du&nbsp;mélange</a>
         <ul>
         <li><a href="$(HOME)/blends/gis/get/live">Téléchargement&nbsp;d’images&nbsp;autonomes</a></li>
         <li><a href="$(HOME)/blends/gis/get/metapackages">Utilisation&nbsp;des&nbsp;métapaquets</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/gis/deriv">Distributions dérivées</a></li>
      <li><a href="https://wiki.debian.org/DebianGIS#Development">Développement</a>
        <ul>
        <li><a href="<gis-policy-html/>">Politique de l’équipe GIS</a></li>
        </ul>
      </li>
    </ul>
   </div>
:#alternate_navbar#}
