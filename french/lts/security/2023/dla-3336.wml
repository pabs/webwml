#use wml::debian::translation-check translation="4f0a0aec9ff61adef5164db4087dae6787f382af" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans node-types-url-parse, un
module Node.js utilisé pour analyser les URL, qui pouvaient aboutir à un
contournement d’autorisation ou une redirection vers des sites non fiables.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3664">CVE-2021-3664</a>

<p>url-parse gérait incorrectement certains usages de (contre)oblique tels
que <code>https:&bsol;</code> et <code>https:/</code>, et interprètait l’URI
comme un chemin relatif. Les navigateurs acceptent une seule contre-oblique
après le protocole et la traitent comme une oblique normale tandis que url-parse
considérait cela comme un chemin relatif. Selon l’utilisation de la bibliothèque
cela pouvait aboutir à un contournement de liste <q>autorisé/bloqué</q>, des
attaques de contrefaçon de requête côté serveur (SSRF), des ouvertures de
redirections et d’autres comportements non désirés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27515">CVE-2021-27515</a>

<p>L’utilisation de contre-oblique dans le protocole était autorisé dans le
navigateur, alors que url-parse pensait que c’était un chemin relatif. Une
application qui validait un URL en utilisant url-parse pouvait passer un lien
malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0512">CVE-2022-0512</a>

<p>Un traitement incorrect du nom d’utilisateur et du mot de passe pouvait
conduire à un échec d’identification correcte du nom d’hôte, qui pouvait alors
aboutir à un contournement d’autorisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0639">CVE-2022-0639</a>

<p>Une conversion incorrecte de caractères <code>@</code> dans le protocole dans
le champ <code>href</code> pouvait conduire à un échec d’identification correcte
du nom d’hôte, qui pouvait alors aboutir à un contournement d’autorisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0686">CVE-2022-0686</a>

<p>Rohan Sharma a signalé que url-parse était incapable de trouver le nom d’hôte
correct quand aucun numéro de port n’était fourni dans l’URL, tel que dans
<code>http://example.com:</code>. Cela pouvait aboutir alors à une attaque
SSRF, une ouverture de redirections ou n’importe quelle autre vulnérabilité qui
dépendait du champ <code>hostname</code> dans l’URL utilisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0691">CVE-2022-0691</a>

<p>url-parse était incapable de trouver le nom d’hôte correct quand l’URL
contenait un caractère avec contre-oblique <code>&bsol;b</code>. Cela conduisait
l’analyseur à interpréter l’URL comme un chemin relatif, contournant toutes les
vérifications de nom d’hôte. Cela pouvait aussi conduire à des faux positifs
dans <code>extractProtocol()</code>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.2.0-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-url-parse.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-url-parse,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-url-parse">\
https://security-tracker.debian.org/tracker/node-url-parse</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3336.data"
# $Id: $
