#use wml::debian::translation-check translation="4a50708f3f930fe854540df9ba3411b62d8aeba3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une régression dans le correctif précédent
pour python-cryptography, une bibliothèque de Python fournissant un certain
nombre de primitives de chiffrement et déchiffrement.</p>

<p>Le problème originel était qu’il existait une vulnérabilité potentielle de
corruption de mémoire. Cependant, la modification a entrainé une régression due
à un rétroportage incorrect du correctif de l’amont vers l’(ancienne) version
dans Debian LTS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23931">CVE-2023-23931</a>

<p>cryptography est un paquet conçu pour fournir des primitives de chiffrement
et des recettes aux développeurs utilisant Python. Dans les versions affectées,
<q>Cipher.update_into</q> acceptait des objets Python qui mettaient en œuvre le
protocole de tampon, mais fournissait seulement des tampons immuables. Cela
permettait aux objets immuables (tels que <q>octets</q>) de muter, par
conséquent violant les règles fondamentales de Python et aboutissant à une
sortie corrompue. Désormais une exception est correctement levée. Ce problème
est présent depuis que <q>update_into</q> a été introduit dans cryptography 1.8.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.6.1-3+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-cryptography.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3331.data"
# $Id: $
