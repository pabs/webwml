#use wml::debian::translation-check translation="0444bbcc931b6fc08720ef7450c1ce27c1833f1e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans nss, les
bibliothèques du Network Security Service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6829">CVE-2020-6829</a>

<p>Lors de la multiplication de points scalaires par courbe elliptique,
l’algorithme wNAF de multiplication de points était utilisé, qui faisait fuiter
des informations partielles sur le nonce utilisé durant la génération de la
signature. Avec une trace électromagnétique de quelques générations de signature,
la clé privée pouvait être calculée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12400">CVE-2020-12400</a>

<p>Lors de la conversion de coordonnées projectives à affines, l’inversion
modulaire n’était pas réalisée à temps constant, aboutissant à une possible
attaque temporelle par canal auxiliaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12401">CVE-2020-12401</a>

<p>Pendant la génération de signature ECDSA, le remplissage appliqué au nonce,
conçu pour assurer une multiplication scalaire à temps constant, a été retiré,
aboutissant à une exécution à temps variable dépendant des données du secret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12403">CVE-2020-12403</a>

<p>Un défaut a été découvert dans la façon dont CHACHA20-POLY1305 a été mis en
œuvre dans NSS. Lors de l’utilisation de Chacha20 multi-part, il pouvait causer des
lectures hors limites. Ce problème a été corrigé en désactivant explicitement
ChaCha20 multi-part (qui ne fonctionnait pas correctement) et en forçant
strictement la longueur des balises.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0767">CVE-2023-0767</a>

<p>Christian Holler a découvert que le traitement incorrect d'attributs
« Safe Bag » de PKCS #12 pouvait avoir pour conséquence l’exécution de
code arbitraire lors du traitement d'un paquet de certificats PKCS #12
contrefaits pour l'occasion.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2:3.42.1-1+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nss,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nss">\
https://security-tracker.debian.org/tracker/nss</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3327.data"
# $Id: $
