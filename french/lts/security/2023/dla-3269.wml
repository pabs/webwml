#use wml::debian::translation-check translation="cb67e16a7c8cb81b8e9fc4ece33100f54d249c81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut dans libapreq2 d’Apache, versions 2.16 et précédentes, pouvait
causer un dépassement de tampon lors du traitement de téléchargements de
formulaires multiparties. Un attaquant distant pouvait envoyer une requête
provoquant un plantage du processus, permettant une attaque par déni de
service.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.13-7~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapreq2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libapreq2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapreq2">\
https://security-tracker.debian.org/tracker/libapreq2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3269.data"
# $Id: $
