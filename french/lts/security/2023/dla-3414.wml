#use wml::debian::translation-check translation="b499960a2f8267f992be583c843271c5f6e0bc77" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de déni de service (DoS)
locale dans Avahi, un système facilitant la découverte de services dans un
réseau local. Le processus avahi-daemon pouvait être planté à travers un bus
de messages DBus.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1981">CVE-2023-1981</a>

<p>avahi-daemon pouvait être planté à l’aide de DBus.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.7-4+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets avahi.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3414.data"
# $Id: $
