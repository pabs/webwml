#use wml::debian::translation-check translation="2cfb81e1a6e4c87d81efe65a30dadf57d4fb2208" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été trouvés dans PHP, un langage de
script généraliste au source libre couramment utilisé, qui pouvaient aboutir à
un déni de service ou une validation incorrecte de hachages BCrypt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31631">CVE-2022-31631</a>

<p>À cause d’un dépassement d'entier non détecté, <code>PDO::quote()</code> de
<code>PDO_SQLite</code> pouvait renvoyer une chaine incorrectement délimitée. Les
détails exacts dépendaient vraisemblablement de l’implémentation de
<code>sqlite3_snprintf()</code>, mais, avec certaines versions, il était
possible de forcer la fonction à renvoyer une seule apostrophe, si la fonction
était appelée sur une entrée fournie par un utilisateur sans restriction de
longueur mise en place.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0567">CVE-2023-0567</a>

<p>Tim Düsterhus a découvert que des hachages BCrypt mal formés qui incluaient
un caractère <code>&dollar;</code> dans leur salage déclenchaient un
dépassement de lecture mémoire et pouvaient valider de manière erronée tout
mot de passe (<code>Password_verify()</code> renvoyait toujours
<code>true</code> avec de telles entrées).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0568">CVE-2023-0568</a>

<p>Surlecture d’un octet de tableau lors de l’ajout de barre oblique dans des
chemins lors de la résolution de chemins.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0662">CVE-2023-0662</a>

<p>Jakob Ackermann a découvert une vulnérabilité de déni de service lors de
l’analyse de corps de requête multipartie : cette analyse dans PHP permettait à
un attaquant non authentifié d’utiliser pendant longtemps le CPU et déclencher
une journalisation excessive.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 7.3.31-1~deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.3">\
https://security-tracker.debian.org/tracker/php7.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3345.data"
# $Id: $
