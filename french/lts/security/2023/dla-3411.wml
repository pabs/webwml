#use wml::debian::translation-check translation="b75b9e4357cb42bb18e1f6e64ef9475ceb7543f2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il s’agit de la mise à jour de la routine de la base de données
distro-info-data pour les utilisateurs de Debian LTS.</p>

<p>Elle inclut la date attendue de publication de Debian 12, ajoute Debian 14,
ajoute Ubuntu 23.10 et quelques mises à jour moins importantes de fin de vie
pour des publications d’Ubuntu.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.41+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets distro-info-data.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de distro-info-data,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/distro-info-data">\
https://security-tracker.debian.org/tracker/distro-info-data</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3411.data"
# $Id: $
