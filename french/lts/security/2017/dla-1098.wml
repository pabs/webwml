#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>L’équipe Cisco Talos a signalé deux problèmes de sécurité sensibles affectant
FreeXL-1.0.3 et n’importe quelles versions précédentes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2923">CVE-2017-2923</a>

<p>Une vulnérabilité exploitable de dépassement de tampon basé sur le tas existe
dans la fonction read_biff_next_record de FreeXL 1.0.3. Un fichier XLS contrefait
spécialement peut causer une corruption de mémoire aboutissant à une exécution
de code à distance. Un attaquant peut envoyer un fichier XLS malveillant pour
déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2924">CVE-2017-2924</a>

<p>Une vulnérabilité exploitable de dépassement de tampon basé sur le tas existe
dans la fonction read_legacy_biff de FreeXL 1.0.3. Un fichier XLS contrefait
spécialement peut causer une corruption de mémoire aboutissant à une exécution
de code à distance. Un attaquant peut envoyer un fichier XLS malveillant pour
déclencher cette vulnérabilité.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.0.0b-1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freexl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1098.data"
# $Id: $
