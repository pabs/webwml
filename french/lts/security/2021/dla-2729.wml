#use wml::debian::translation-check translation="0aae76304145f23ac20c3862a5020aa58e203f10" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans le système de téléphonie
Asterisk. Si le pilote de canal IAX2 recevait un paquet qui contenait un
format de média non pris en charge, un plantage pouvait se produire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32558">CVE-2021-32558</a>

<p>Un problème a été découvert dans les versions de Sangoma d’Asterisk, 13.x
avant 13.38.3, 16.x avant 16.19.1, 17.x avant 17.9.4 et 18.x avant 18.5.1
et de Certified Asterisk avant 16.8-cert10. Si le pilote de canal
IAX2 recevait un paquet qui contenait un format de média non pris en charge,
un plantage pouvait se produire.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:13.14.1~dfsg-2+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>De plus, Asterisk suit ce problème sous la référence <tt>AST-2021-008</tt></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2729.data"
# $Id: $
