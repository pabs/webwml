#use wml::debian::translation-check translation="7b93b39e050186a6d8f53281a49147e90a5d260b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Roman Fiedler a découvert une vulnérabilité dans le code OverlayFS de
firejail, un programme de bac à sable pour restreindre l'environnement
d'exécution d'applications non approuvées, qui pourrait avoir pour conséquence
une élévation de privilèges administrateur. Cette mise à jour désactive la prise
en charge d'OverlayFS dans firejail.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.9.44.8-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firejail.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firejail, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firejail">\
https://security-tracker.debian.org/tracker/firejail</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2554.data"
# $Id: $
