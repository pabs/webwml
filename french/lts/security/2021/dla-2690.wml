#use wml::debian::translation-check translation="106765be5c12c3d91f3bb55ed9e468d96ef40d0e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à l'exécution de code arbitraire, une élévation des
privilèges, un déni de service ou une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24586">CVE-2020-24586</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2020-24587">CVE-2020-24587</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2020-26147">CVE-2020-26147</a>

<p>Mathy Vanhoef a découvert que beaucoup d’implémentations de Wi-Fi, y compris
mac80211, par Linux ne mettaient pas en œuvre le réassemblage de paquets
fragmentés. Dans certaines circonstances, un attaquant dans le périmètre d’un
réseau pourrait exploiter ces défauts pour fabriquer des paquets arbitraires ou
pour accéder à des données sensibles sur ce réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24588">CVE-2020-24588</a>

<p>Mathy Vanhoef a découvert que beaucoup d’implémentations de Wi-Fi, y compris
mac80211, par Linux n’authentifiaient pas l’indicateur d’en-tête de paquet
<q>agrégé</q>. Un attaquant dans le périmètre d’un réseau pourrait exploiter ces
défauts pour fabriquer des paquets arbitraires sur ce réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25670">CVE-2020-25670</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2020-25671">CVE-2020-25671</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-23134">CVE-2021-23134</a>

<p>Kiyin (尹亮) de TenCent a découvert plusieurs bogues de comptage de
références dans l’implémentation NFC de LLCP qui pourraient conduire à une
utilisation de mémoire après libération. Un utilisateur local pourrait exploiter
cela pour un déni de service (plantage ou corruption de mémoire) ou
éventuellement pour une élévation des privilèges.</p>

<p>Nadav Markus et Or Cohen de Palo Alto Networks ont découvert que les
correctifs originaux pour cela introduisaient un nouveau bogue qui pourrait
aboutir à une utilisation de mémoire après libération et une double libération
de zone de mémoire. La correction a été aussi faite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25672">CVE-2020-25672</a>

<p>Kiyin (尹亮) de TenCent a découvert une fuite de mémoire dans
l’implémentation NFC de LLCP. Un utilisateur local pourrait exploiter cela pour
un déni de service (épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26139">CVE-2020-26139</a>

<p>Mathy Vanhoef a découvert un bogue dans quelques implémentations de Wi-Fi, y
compris mac80211 de Linux. Lors d’un fonctionnement en mode AP, elles
retransmettaient des trames EAPOL d’un client vers un autre alors que
l’expéditeur n’était pas encore authentifié. Un attaquant dans le périmètre d’un
réseau pourrait utiliser cela pour un déni de service ou pour une aide pour
exploiter d’autres vulnérabilités.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26558">CVE-2020-26558</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-0129">CVE-2021-0129</a>

<p>Des chercheurs de l’ANSSI ont découvert des vulnérabilités dans la méthode
d’authentification Passkey de Bluetooth et dans son implémentation par Linux.
Un attaquant dans le périmètre de deux périphériques Bluetooth, lors de
l’appariement avec l’authentification Passkey, pourrait exploiter cela pour
obtenir le secret partagé (Passkey) et alors se faire passer pour l’un des
périphérique auprès de l’autre.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29374">CVE-2020-29374</a>

<p>Jann Horn de Google a signalé un défaut dans la gestion de la mémoire
virtuelle de Linux. Des processus parent ou enfant partagent initialement leur
mémoire, mais lorsqu’ils écrivent sur une page partagée, la page est dupliquée
et non partagée (copie sur écriture). Cependant, dans le cas d’une opération
telle que vmsplice() nécessitant que le noyau ajoute une référence à une page
partagée, et qu’une opération copie sur écriture se produit pendant cette
opération, le noyau pourrait avoir accès à la mauvaise mémoire du processus.
Pour quelques programmes, cela pourrait conduire à une fuite d'informations ou
à une corruption de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3483">CVE-2021-3483</a>

<p>马哲宇 (Zheyu Ma) a signalé un bogue dans le pilote <q>nosy</q> pour les
contrôleurs PCILynx FireWire, qui pourrait conduire à une corruption de liste
et une utilisation de mémoire après libération. Sur un système utilisant ce
pilote, des utilisateurs locaux avec accès à /dev/nosy pourraient exploiter
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou
éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3506">CVE-2021-3506</a>

<p>ADLab de venustech a découvert un bogue dans le pilote F2FS qui pourrait
conduire à une lecture hors limites lors de l’accès à un système de fichiers
contrefait. Un utilisateur local autorisé à monter un système de fichiers
arbitraire pourrait exploiter cela pour provoquer un déni de service (plantage)
ou un autre impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3564">CVE-2021-3564</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-3573">CVE-2021-3573</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-32399">CVE-2021-32399</a>

<p>L’équipe BlockSec a découvert plusieurs situations de compétition dans le
sous-système Bluetooth qui pourraient conduire à une utilisation de mémoire
après libération ou une double libération de zone de mémoire. Un utilisateur
local pourrait exploiter cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3587">CVE-2021-3587</a>

<p>Active Defense Lab de Venustech a découvert un potentiel déréférencement de
pointeur NULL dans l’implémentation NFC de LLCP. Un utilisateur local
pourrait utiliser cela pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23133">CVE-2021-23133</a>

<p>Or Cohen de Palo Alto Networks a découvert une situation de compétition dans
l’implémentation de SCTP, qui pourrait conduire à une corruption de liste. Un
utilisateur local pourrait exploiter cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou éventuellement pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28688">CVE-2021-28688</a> (<a href="https://xenbits.xen.org/xsa/advisory-371.html">XSA-371</a>)

<p>Il a été découvert que le correctif original pour
<a href="https://security-tracker.debian.org/tracker/CVE-2021-26930">CVE-2021-26930</a>
(<a href="https://xenbits.xen.org/xsa/advisory-365.html">XSA-365</a>)
introduisait une fuite de ressources. Un invité malveillant pourrait
probablement exploiter cela afin de provoquer un déni de service
(épuisement de ressources) dans l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28964">CVE-2021-28964</a>

<p>Zygo Blaxell a signalé une situation de compétition dans le pilote Btrfs qui
pourrait conduire à un échec d’assertion. Sur les systèmes utilisant Btrfs, un
utilisateur local pourrait exploiter cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28971">CVE-2021-28971</a>

<p>Vince Weaver a signalé un bogue dans le gestionnaire de performance
d’évènement pour PEBS d’Intel. Un palliatif pour un bogue matériel sur les CPU
d’Intel, nom de code <q>Haswell</q> et précédents, pourrait conduire à un
déréférencement de pointeur NULL. Sur les systèmes avec les CPU en cause, si des
utilisateurs ont accès aux évènements de performance, un utilisateur local
pourrait exploiter cela pour provoquer un déni de service (plantage).</p>

<p>Par défaut, les utilisateurs non privilégiés n’ont pas accès aux évènements
de performance, ce qui atténue ce problème. Ils sont contrôlés par l’interface
sysctl kernel.perf_event_paranoid sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29154">CVE-2021-29154</a>

<p>Il a été découvert que le compilateur JIT Extended BPF (eBPF) pour x86_64
générait des instructions de branche incorrectes dans certains cas. Sur les
système où JIT eBPF est activé, des utilisateurs pourraient exploiter cela pour
exécuter du code arbitraire dans le noyau.</p>

<p>Par défaut, JIT eBPF est désactivé, atténuant ce problème. Cela est contrôlé
par l’interface sysctl net.core.bpf_jit_enable sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29155">CVE-2021-29155</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-31829">CVE-2021-31829</a>

<p>Piotr Krysiuk et Benedict Schlueter ont découvert que le vérificateur
Extended BPF (eBPF) ne protégeait pas entièrement d’une fuite d’informations
à cause d’une exécution spéculative. Un utilisateur local pourrait exploiter
cela pour obtenir des informations sensibles de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29264">CVE-2021-29264</a>

<p>Il a été découvert que le pilote <q>gianfar</q> Ethernet, utilisé avec
quelques SoC Freescale, ne gérait pas correctement un dépassement de file
d’attente Rx quand les paquets <q>jumbo</q> étaient activés. Sur les systèmes
utilisant ce pilote et des paquets jumbo, un attaquant sur le réseau pourrait
exploiter cela pour provoquer un déni de service (plantage).</p>

<p>Ce pilote n’est pas activé dans les configurations officielles du noyau
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29647">CVE-2021-29647</a>

<p>L’outil syzbot a trouvé une fuite d'informations dans l’implémentation de
routeur IPC Qualcomm (qrtr).</p>

<p>Ce protocole n’est pas activé dans les configurations officielles du noyau
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29650">CVE-2021-29650</a>

<p>Il a été découvert qu’une compétition de données dans le sous-système
netfilter pourrait conduire à un déréférencement de pointeur NULL lors du
remplacement de table. Un utilisateur local avec la capacité CAP_NET_ADMIN dans
n’importe quel espace de noms utilisateur pourrait utiliser cela pour provoquer
un déni de service (plantage).</p>

<p>Par défaut, les utilisateurs non privilégiés ne peuvent pas créer un espace
de noms utilisateur, ce qui atténue ce problème. Cela est contrôlé par
l’interface sysctl kernel.unprivileged_userns_clone.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31916">CVE-2021-31916</a>

<p>Dan Carpenter a signalé une validation incorrecte de paramètre dans le
sous-système device-mapper (dm), qui pourrait conduire à un dépassement de
tampon de tas. Cependant, seuls les utilisateurs avec la capacité CAP_SYS_ADMIN
(c’est-à-dire, équivalents au superutilisateur) pourraient déclencher ce bogue,
aussi cela n’a pas d’impact de sécurité dans cette version du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33034">CVE-2021-33034</a>

<p>L’outil syzbot a trouvé un bogue dans le sous-système Bluetooth qui pourrait
conduire à une utilisation de mémoire après libération. Un utilisateur local
pourrait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou éventuellement pour une élévation des privilèges.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.19.194-1~deb9u1. De plus, cette mise à jour corrige les bogues
Debian <a href="https://bugs.debian.org/986949">n° 986949</a>,
<a href="https://bugs.debian.org/988352">n° 988352</a> et
<a href="https://bugs.debian.org/989451">n° 989451</a> et inclut de nombreux
autres corrections de bogues à partir des mises à jour des versions stables,
n° 4.9.259-4.9.272 incluse.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">\
https://security-tracker.debian.org/tracker/linux-4.19</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2690.data"
# $Id: $
