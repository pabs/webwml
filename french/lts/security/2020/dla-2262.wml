#use wml::debian::translation-check translation="d865f76ebc86ab1cdd68218b3fbc1c9291f641fd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été corrigées dans qemu, un émulateur rapide de
processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1983">CVE-2020-1983</a>

<p>slirp : correction de l’utilisation de mémoire après libération dans ip_reass().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13361">CVE-2020-13361</a>

<p>es1370_transfer_audio dans hw/audio/es1370.c permettait à des utilisateurs d’OS
invité de déclencher un accès hors limites lors d’une opération es1370_write().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13362">CVE-2020-13362</a>

<p>megasas_lookup_frame dans hw/scsi/megasas.c possédait une lecture hors limites
à l'aide d'un champ reply_queue_head contrefait d’un utilisateur d’OS invité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13765">CVE-2020-13765</a>

<p>hw/core/loader : correction d’un plantage possible dans rom_copy().</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:2.1+dfsg-12+deb8u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2262.data"
# $Id: $
