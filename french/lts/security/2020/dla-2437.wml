#use wml::debian::translation-check translation="f4afbd9670dcda61c9ae28c354a7edcba31d3380" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de déni de service dans
le système d’authentification réseau Kerberos du MIT, <tt>krb5</tt>. L’absence
de limite dans le décodeur ASN.1 pourrait conduire à une récursion infinie et
permettre à un attaquant de déborder la pile et provoquer le plantage du
processus.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28196">CVE-2020-28196</a></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.15-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets krb5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2437.data"
# $Id: $
