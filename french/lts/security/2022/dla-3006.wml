#use wml::debian::translation-check translation="c89bbf6d362794c7bf461e1749fe64fe2c77d81e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'environnement
d'exécution Java OpenJDK, qui peuvent avoir pour conséquences la
divulgation d'informations ou un déni de service.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 8u332-ga-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjdk-8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjdk-8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjdk-8">\
https://security-tracker.debian.org/tracker/openjdk-8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3006.data"
# $Id: $
