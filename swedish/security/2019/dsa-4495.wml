#use wml::debian::translation-check translation="2b74abc752f8e4232fe85da5b7c01782113a2f4d" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Linuxkärnan som kan leda till
utökning av privilegier, överbelastning eller informationsläckage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

	<p>chenxiang rapporterade en kapplöpningseffekt i libsas, kärnans
	undersystem för stöd för Serial Attached SCSI-enheter (SAS), vilket
	kunde leda till en användning efter frigörning. Det är inte klart hur detta
	kunde utnyttjas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

	<p>Man har upptäckt att de flesta x86-processorerna kunde spekulativt
	hoppa över en villkorlig SWAPGS-instruktion som används när man går in
	i kärnan från user-läge, och/eller kunde spekulativt exekvera den när
	den skulle hoppas över. Detta är en undertyp av Spectre variant 1,
	vilket kunde tillåta lokala användare att få känslig information från
	kärnan eller andra processer. Den har lindrats genom att använda
	minnesbarriärer för att begränsa spekulativ exekvering. System som använder
	en i386-kärna påverkas inte eftersom den inte använder SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1999">CVE-2019-1999</a>

	<p>En kapplöpningseffekt har upptäckts i Android binder drivrutinen,
	vilket kunde leda till en användning efter frigörning. Om denna drivrutin
	laddas, kan en lokal användare ha möjlighet att använda detta för
	överbelastning (minneskorruption) eller för utökning av privilegier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

	<p>Verktyget syzkaller upptäckte en potentiell null-dereferens i olika
	drivrutiner för UART-anslutna Bluetoothadaptrar. En lokal användare
	med tillgång till pty-enheten eller andra lämpliga tty-enheter kunde
	använda detta för överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

	<p>Amit Klein och Benny Pinkas upptäckte att genereringen av
	IP-paket-ID använde en svag hash-funktion, <q>jhash</q>. Detta kunde
	tillåta att spåra individuella datorer när de kommunicerar med
	olika fjärrservrar och från olika nätverk. Nu används istället funktionen
	<q>siphash</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12817">CVE-2019-12817</a>

	<p>Man har upptäckt att hash page table (HPT)-koden inte hanterade
	fork() i en process med minne mappat till adresser över 512 TiB korrekt på
	PowerPC (ppc64el)-arkitekturen. Detta kunde leda till användning efter
	frigörning i kärnan eller icke avsedd delning av minne mellan processer.
	En lokal användare kunde utnyttja detta för utökning av privilegier.
	System som använder sig av radix MMU, eller en anpassad kärna med en
	4 KiB sidstorlek påverkas inte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12984">CVE-2019-12984</a>

	<p>Man har upptäckt att implementationen av NFC-protokollet inte
	validerar en netlinkkontrollmeddelande ordentligt, vilket potentiellt
	kan leda till en nullpekardereferens. En lokal användare på ett system
	med ett NFC-gränssnitt kunde använda detta för överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13233">CVE-2019-13233</a>

	<p>Jann Horn upptäckte en kapplöpningseffekt på x86-arkitekturen,
	vid användning av LDTn. Detta kunde leda till en användning efter
	frigörning. En lokal användare kunde möjligen använda detta för
	överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

	<p>Man har upptäckt att drivrutinen för gtco för USB-inputtablets kunde
	orsaka ett stackbuffertspill med konstant data vid tolkning av enhetens
	beskrivare. En fysiskt närvarande användare med en speciellt skapad
	USB-enhet kunde utnyttja detta för att orsaka en överbelastning eller
	möjligen en utökning av privilegier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

	<p>Praveen Pandey rapporterade att på PowerPC-system (ppc64el) utan
	Transactional Memory (TM), kom kärnan att fortfarande försöka att
	återställa TM-läge som skickades till systemanropet sigreturn(). En
	lokal användare kunde utnyttja detta för överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

	<p>Verktyget syzkaller upptäckte en saknad gränskontroll i drivrutinen
	för floppydisk. En lokal användare med åtkomst till en floppy-enhet,
	med en disk i, kunde utnyttja detta för att läsa kärnminne bortom
	I/O-buffern, och därmed möjligen få tag på känslig information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

	<p>Verktyget syzkaller upptäckte en potentiell division med noll i
	floppydisk-drivrutinen. En lokal användare med åtkomst till en
	floppydiskenhet kunde utnyttja detta för överbelastning.</p></li>

</ul>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 4.19.37-5+deb10u2.</p>

<p>För den gamla stabila utgåvan (Stretch) kommer dessa problem att fixed
inom kort.</p>

<p>Vi rekommenderar att ni uppgraderar era linux-paket.</p>

<p>För detaljerad säkerhetsstatus om linux vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4495.data"
