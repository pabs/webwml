#use wml::debian::template title="Tack för att du hämtar Debian!" 
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="9726492496ae1d4cebd3f501944b1459a43788aa"

<p>Detta är Debian <:=substr '<current_initial_release>', 0, 2:>, med kodnamnet <em><current_release_name></em>, för nätverksinstallation, för <: print $arches{'amd64'}; :>
<a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>

<p>Kontrollsumma för hämtningen: <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a> <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Signatur</a></p>



<div class="tip">
	<p><strong>Viktigt</strong>: Säkerställ att du <a href="$(HOME)/CD/verify">verifierar din hämtning med kontrollsumman</a>.</p>
</div>

<p>Debians installations-ISOs är hybrid-avbildningar, vilket betyder att dom kan skrivas direkt till CD/DVD/BD eller till <a href="https://www.debian.org/CD/faq/#write-usb">USB-minnen</a>.</p>

<h2 id="h2-1">
	Andra installerare</h2>

<p>Andra installerare och avbildningar, så som livesystem, offlineinstallerare för system utan nätverksanslutning, installerare för andra CPU-arkitekturer, eller molninstanser, kan hittas på <a href="$(HOME)/distrib/">Få tag på Debian</a>.</p>

<p>Inofficiella installerare med <a href="https://wiki.debian.org/Firmware"><strong>icke-fri fastprogramvara (firmware)</strong></a>, som är användbart för vissa nätverks- och videoadaptrar, kan hämtas från <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Inofficiella icke-fria avbildningar som inkluderar fastprogramvarupaket</a>.</p>

<h2 id="h2-2">Relaterade länkar</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Installationsguide</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Versionsfakta</a></p>
<p><a href="$(HOME)/CD/verify">ISO-verifikationsguide</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Alternativa hämtningssajter</a></p>
<p><a href="$(HOME)/releases">Andra utgåvor</a></p>


