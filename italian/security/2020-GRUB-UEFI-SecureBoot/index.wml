#use wml::debian::template title="Vulnerabilità UEFI SecureBoot con GRUB2: <q>BootHole</q>"
#use wml::debian::translation-check translation="9c47c948da90cea57112872aa23f03da3a967d7b"

<p>
Gli sviluppatori di Debian e di altri progetti nella comunità Linux sono
recentemente venuti a conoscenza di un grave problema nel bootloader GRUB2
che consente di aggirare completamente l'UEFI SecureBoot. Tutti i  dettagli
sul problema sono descritti nel <a
href="$(HOME)/security/2020/dsa-4735">bollettino di sicurezza Debian
4735</a>. Lo scopo di questo documento è spiegare le conseguenze di questa
vulnerabilità della sicurezza e quali passi devono essere fatti per
affrontarla.
</p>

<ul>
  <li><b><a href="#what_is_SB">Premessa: cos'è UEFI SecureBoot?</a></b></li>
  <li><b><a href="#grub_bugs">Trovati dei bug in GRUB2</a></b></li>
  <li><b><a href="#linux_bugs">Trovati dei bug anche in Linux</a></b></li>
  <li><b><a href="#revocations">È necessario revocare le chiavi per riparare la catena di Secure Boot</a></b></li>
  <li><b><a href="#revocation_problem">Quali sono gli effetti della revoca della chiave?</a></b></li>
  <li><b><a href="#package_updates">Pacchetti aggiornati</a></b>
  <ul>
    <li><b><a href="#grub_updates">1. GRUB2</a></b></li>
    <li><b><a href="#linux_updates">2. Linux</a></b></li>
    <li><b><a href="#shim_updates">3. Shim</a></b></li>
    <li><b><a href="#fwupdate_updates">4. Fwupdate</a></b></li>
    <li><b><a href="#fwupd_updates">5. Fwupd</a></b></li>
  </ul></li>
  <li><b><a href="#buster_point_release">Rilascio minore Debian 10.5
       (<q>buster</q>), supporti per l'installazione e live
	   aggiornati</a></b></li>
  <li><b><a href="#more_info">Ulteriori informazioni</a></b></li>
</ul>

<h1><a name="what_is_SB">Premessa: cos'è UEFI SecureBoot?</a></h1>

<p>
Lo UEFI Secure Boot (SB) è un meccanismo di verifica che assicura che il
codice eseguito da un computer con firmware UEFI sia attendibile. È
progettato per proteggere un sistema dal caricamento e dall'esecuzione
di codice malevolo nelle prime fasi del processo di avvio, prima che sia
caricato il sistema operativo.
</p>

<p>
SB funziona utilizzando somme di controllo e firme crittografiche. Ogni
programma che viene caricato dal firmware contiene una firma e una
somma di controllo, prima che ne sia concessa l'esecuzione il firmware
controlla che il programma sia attendibile validandone la somma di
controllo e la firma. Quando su un sistema è attivo il SB è impedito
qualunque tentativo di eseguire un programma non attendibile. Ciò
impedisce di eseguire codice inaspettato / non autorizzato nell'ambiente
UEFI.
</p>

<p>
La gran parte dell'hardware x86 viene precaricato dal costruttore con le
chiavi di Microsoft. Ciò comporta che il firmware di tali sistemi riterr&agrave;
attendibile i binari firmati da Microsoft. I sistemi più recenti sono
consegnati con il SB attivato, con le impostazioni predefinite non sono
in grado di eseguire codice non firmato tuttavia è possibile cambiare la
configurazione del firmware e disattivare il SB oppure registrare delle
chiavi di firma aggiuntive.
</p>

<p>
Debian, come molti altri sistemi operativi basati su Linux, utilizza un
programma chiamato shim per ampliare la fiducia del firmware agli altri
programmi che devono essere sicuri nella prima fase dell'avvio: il
bootloader GRUB2, il kernel Linux e gli strumenti per l'aggiornamento
del firmware (fwupd e fwupdate).
</p>

<h1><a name="grub_bugs">Trovati dei bug in GRUB2</a></h1>

<p>
Purtroppo è stato trovato un grave bug nel codice del bootloader GRUB2 quando
legge e analizza la propria configurazione (grub.cfg). Questo bug spezza la
catena della fiducia, sfruttando questo bug è possibile uscire dall'ambiente
protetto e caricare dei programmi non firmati nella prima fase di avvio.
Queste vulnerabilità è stata scoperta da dei ricercatori di Eclypsium e gli è
stato dato nome <b><a 
href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">BootHole</a></b>.
</p>

<p>
Anziché semplicemente correggere il bug, gli sviluppatori sono stati spinti
a fare un'accurata revisione del codice sorgente di GRUB2. Sarebbe stato
irresponsabile correggere una falla senza cercarne altre! Una squadra di
ingegneri hanno lavorato assieme per parecchie settimane per individuare e
sistemare una varietà di problemi. Sono stati trovati alcuni posti in cui,
con un input inaspettato, le allocazioni di memoria interna potevano andare
in overflow, in molti altri posti in cui calcoli matematici potevano andare
in overflow e causare problemi e in qualche altro posto in cui la memoria
avrebbe potuto essere utilizzata dopo che era stata liberata. Tutte le
correzioni sono state condivise e testate dalla comunità.
</p>

<p>
Nuovamente, consultare il <a href="$(HOME)/security/2020/dsa-4735">bollettino
di sicurezza Debian 4735</a> per l'elenco completo dei problemi trovati.
</p>


<h1><a name="linux_bugs">Trovati dei bug anche in Linux</a></h1>

<p>
Mentre si discuteva delle falle di GRUB2, gli sviluppatori hanno
parlato anche di due modi recentemente trovati da Jason A. Donenfeld
(zx2c4) (<a
href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language.sh">1</a>,
<a href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language-2.sh">2</a>)
in cui anche Linux avrebbe potuto consentire di aggirare il Secure Boot.
Entrambi permettono a root di rimpiazzare le tabelle ACPI su un sistema
blindato nonostante ciò non debba essere permesso. Le correzioni sono gi&agrave;
state rilasciate per questi problemi.
</p>

<h1><a name="revocations">È necessario revocare le chiavi per riparare
la catena di Secure Boot</a></h1>

<p>
Debian e gli altri produttori di sistemi operativi ovviamente <a
href="#package_updates">rilasceranno le versioni corrette</a> di GRUB2 e
Linux. Tuttavia ciò non è una soluzione completa a tutti i problemi visti.
Dei malintenzionati potrebbero essere ancora in grado di usare le vecchie
e vulnerabili versioni per aggirare il Secure Boot.
</p>

<p>
Per impedire ciò, il prossimo passo sarà per Microsoft di bloccare i binari
non sicuri in modo da impedirne l'esecuzione con SB. Questo si ottiene
utilizzando l'elenco <b>DBX</b>, una funzionalità prevista nel modello di
UEFI Secure Boot. A tutte le distribuzioni che contengono una copia di shim
firmata con le Microsoft è richiesto di fornire i dettagli dei binari o
delle chiavi coinvolte per facilitare questa procedura. Nella <a
href="https://uefi.org/revocationlistfile">lista di revoca UEFI</a> verranno
inserite queste informazioni. In un momento futuro, <b>non ancora stabilito</b>,
i sistemi inizieranno a utilizzare la lista aggiornata e rifiuteranno di
eseguire i binari vulnerabili con Secure Boot.
</p>

<p>
Il momento <i>esatto</i> in cui questo cambiamento verrà distribuito non è
ancora chiaro; a un certo punto i produttori di BIOS/UEFI includeranno la
nuova lista di revoca nelle nuove realizzazioni di firmware per il nuovo
hardware. Microsoft <b>potrebbe</b> anche rilasciare aggiornamenti per i
sistemi esistenti tramite Windows Update. Alcune distribuzioni Linux
potrebbero rilasciare aggiornamenti tramite il proprio processo per gli
aggiornamenti di sicurezza. Debian <b>per adesso</b> non lo fa, ma lo stiamo
valutando per il futuro.
</p>

<h1><a name="revocation_problem">Quali sono gli effetti della revoca della chiave?</a></h1>

<p>
Molti produttori sono cauti sull'applicazione automatica degli
aggiornamenti che revocano le chiavi usate nel Secure Boot. Le installazioni
esistenti con SB attivo potrebbero immediatamente e assolutamente rifiutare
l'avvio a meno che l'utente sia stato attento e abbia installato anche tutti
gli aggiornamenti ai programmi. I sistemi con doppio avvio Windows/Linux
potrebbero improvvisamente nono avviare più Linux. I vecchi supporti per
l'installazione o di sistemi live naturalmente non riusciranno a partire,
potenzialmente facendo diventare più difficile il ripristino di un sistema.
</p>

<p>
Ci sono due modi ovvi per rimediare a un sistema che non si avvia:
</p>

<ul>
<li>Riavviare in modalità <q>ripristino</q> (rescue) tramite un <a
href="#buster_point_release">supporto per l'installazione recente</a> e
applicare gli aggiornamenti da quello; oppure</li>
<li>Disattivare temporaneamente il Secure Boot per ottenere di nuovo accesso
al sistema, applicare gli aggiornamenti e riattivarlo.</li>
</ul>

<p>
Entrambe le soluzioni sembrano semplici ma ciascuna potrebbe richiedere
molto tempo agli utenti che hanno più sistemi. Inoltre prestare attenzione
al fatto che per attivare e disattivare il Secure Boot è necessario
l'accesso diretto alla macchina, normalmente <b>non</b> è possibile
cambiare questa configurazione al di fuori del firmware del computer.
Proprio per questo motivo le macchine server remote richiedono una
particolare attenzione.
</p>

<p>
Per queste ragioni si raccomanda caldamente a <b>tutti</b> gli utenti
Debian di prestare particolare attenzione a installare tutti gli <a
href="#package_updates">aggiornamenti raccomandati</a> per i propri
sistemi il prima possibile per ridurre la possibilità di avere problemi
in futuro.
</p>

<h1><a name="package_updates">Pacchetti aggiornati</a></h1>

<p>
<b>Nota:</b> i sistemi con Debian 9 (<q>stretch</q>) oppure una versione
più vecchia <b>non</b> riceveranno obbligatoriamente un aggiornamento,
perché Debian 10 (<q>buster</q>) è stato il primo rilascio di Debian a
includere il supporto per Secure Boot di UEFI.
</p>

<p>
Sono state aggiornate le versioni firmate di tutti i pacchetti, persino
di quelli che non avevamo bisogno di modifiche. Debian ha dovuto rigenerare
una nuova chiave/certificato per i propri pacchetti legati a Secure Boot.
Il precedente certificato era chiamato <q>Debian Secure Boot Signer</q>
(fingerprint
<code>f156d24f5d4e775da0e6a9111f074cfce701939d688c64dba093f97753434f2c</code>);
il nuovo è <q>Debian Secure Boot Signer 2020</q>
(<code>3a91a54f9f46a720fe5bbd2390538ba557da0c2ed5286f5351fe04fff254ec31</code>).
</p>

<p>
Sono cinque i pacchetti sorgente in Debian che sono stati aggiornati per
Secure Boot di UEFI con le modifiche descritte di seguito:
</p>

<h2><a name="grub_updates">1. GRUB2</a></h2>

<p>
Le versioni aggiornate dei pacchetti Debian GRUB2 sono adesso disponibili
tramite l'archivio debian-security per il rilascio stabile Debian 10
(<q>buster</q>). Le versioni corrette saranno nel normale archivio delle
versioni di sviluppo di Debian (unstable e testing) molto presto.
</p>

<h2><a name="linux_updates">2. Linux</a></h2>

<p>
Le versioni aggiornate dei pacchetti Debian linux sono adesso disponibili
tramite buster-proposed-updates per il rilascio stabile Debian 10
(<q>buster</q>) e saranno inserite nel prossimo rilascio minore 10.5.
I nuovi pacchetti sono pure nell'archivio Debian delle versioni di
sviluppo (unstable e testing). Ci aspettiamo che presto i pacchetti con le
correzioni siano caricati in buster-backports.
</p>

<h2><a name="shim_updates">3. Shim</a></h2>

<p>
Per come funziona la gestione delle chiavi per il Secure Boot di Debian,
<b>non</b> è necessario revocare i pacchetti di shim esistenti e firmati
da Microsoft. Tuttavia le versioni firmate dei pacchetti shim-helper
devono essere ricostruiti per usare la nuova chiava di firma.
</p>

<p>
Le versioni aggiornate dei pacchetti Debian shim sono adesso disponibili
tramite buster-proposed-updates per il rilascio stabile Debian 10
(<q>buster</q>) e saranno inserite nel prossimo rilascio minore 10.5.
I nuovi pacchetti sono pure nell'archivio Debian delle versioni di
sviluppo (unstable e testing).
</p>

<h2><a name="fwupdate_updates">4. Fwupdate</a></h2>

<p>
Le versioni aggiornate dei pacchetti Debian fwupdate sono adesso disponibili
tramite buster-proposed-updates per il rilascio stabile Debian 10
(<q>buster</q>) e saranno inserite nel prossimo rilascio minore 10.5.
Tempo fa il pacchetto fwupdate è stato rimosso da unstable e testing in
favore di fwupd.
</p>

<h2><a name="fwupd_updates">5. Fwupd</a></h2>

<p>
Le versioni aggiornate dei pacchetti Debian fwupd sono adesso disponibili
tramite buster-proposed-updates per il rilascio stabile Debian 10
(<q>buster</q>) e saranno inserite nel prossimo rilascio minore 10.5.
I nuovi pacchetti sono pure nell'archivio Debian delle versioni di
sviluppo (unstable e testing).
</p>

<h1><a name="buster_point_release">Rilascio minore Debian 10.5 (<q>buster</q>), 
supporti per l'installazione e live aggiornati</a></h1>

<p>
Tutte le correzioni descritte sono destinate a essere inserite nel rilascio
minore Debian 10.5 (<q>buster</q>), previsto per il 1 agosto 2020. Di
conseguenza 10.5 diventa la scelta obbligata per i supporti di
installazione e di sistemi live. In futuro, appena pubblicate le revoche,
le immagini precedenti potrebbero non funzionare con Secure Boot.
</p>

<h1><a name="more_info">Ulteriori informazioni</a></h1>

<p>
Molte altre informazioni sulla configurazione di UEFI Secure Boot con Debian
sono nel wiki, consultare <a
href="https://wiki.debian.org/SecureBoot">https://wiki.debian.org/SecureBoot</a>.</p>

<p>
Altre risorse su questo argomento:
</p>

<ul>
  <li><a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">Articolo
      di Eclypsium <q>BootHole</q></a> in cui sono descritte le vulnerabilit&agrave;
	  trovate</li>
  <li><a href="https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/ADV200011">Microsoft
      Guidance for Addressing Security Feature Bypass in GRUB</a></li>
  <li><a href="https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/GRUB2SecureBootBypass">Articolo
      della KnowledgeBase di Ubuntu</a></li>
  <li><a href="https://access.redhat.com/security/vulnerabilities/grub2bootloader">Articolo
      di Red Hat sulla vulnerabilità</a></li>
  <li><a href="https://www.suse.com/c/suse-addresses-grub2-secure-boot-issue/">Articolo
      di SUSE sulla vulnerabilità</a></li>
</ul>
