#use wml::debian::template title="Erros nos modelos Debconf traduzidos"
#include "$(ENGLISHDIR)/international/l10n/po-debconf/menu.inc"
#use wml::debian::translation-check translation="b339a77580e06b206bedadd0ee4df7dd5cae5ef8"

<p>
Você pode encontrar uma <a href="errors-by-pkg">lista em ordem alfabética dos
pacotes</a> com erros nas traduções dos modelos Debconf. A mesma lista também
está classificada por <a href="errors-by-maint">mantenedores(as)</a>.
</p>

<p>
Para economizar espaço, ambas as listas contêm palavras-chave em vez de
mensagens mais completas, e essas palavras-chave estão definidas aqui. Alguns
desses erros devem ser corrigidos pelos(as) tradutores(as), enquanto outros
devem ser corrigidos pelo(a) mantenedor(a).
</p>

<h3>Erros que são de responsabilidade dos(as) tradutores(as)</h3>

<dl>
  <dt><a name="charsetname">invalid-charset-name-in-po</a></dt>
  <dd>
    Arquivos PO devem possuir um conjunto de caracteres válido no campo de
    cabeçalho "Content-Type:". O conjunto de caracteres é uma escolha dos(as)
    tradutores(as) e o arquivo PO deve, portanto, ser corrigido pelos(as)
    próprios(as) tradutores(as). A menos que os(as) mantenedores(as) tenham
    absoluta certeza do que estejam fazendo.
  </dd>
  <dt><a name="charset">wrong-charset</a></dt>
  <dd>
    O conjunto de caracteres definido no campo "Content-Type:" do cabeçalho PO
    não é o mesmo daquele usado no arquivo PO. O conjunto de caracteres é uma
    escolha dos(as) tradutores(as) e o arquivo PO deve, portanto, ser corrigido
    pelos(as) próprios(as) tradutores(as). A menos que os(as) mantenedores(as)
    tenham absoluta certeza do que estejam fazendo. Esses arquivos não são
    utilizáveis, os(as) mantenedores(as) devem pedir aos(às) tradutores(as) por
    arquivos corrigidos.
  </dd>
  <dt><a name="invalidpo">invalid-po</a></dt>
  <dd>
    O arquivo PO não é válido. As razões podem ser várias. A saída de msgfmt
    deve ajudar ao(à) tradutor(a) a corrigir esses arquivos. Esses arquivos não
    são utilizáveis, os(as) mantenedores(as) devem pedir aos(às) tradutores(as)
    que eles sejam corrigidos
  </dd>
</dl>

<h3>Erros que são de responsabilidade dos(as) mantenedores(as)</h3>

<dl>
  <dt><a name="unknownlanguage">unknown-language</a></dt>
  <dd>
    Um arquivo PO possui um código de idioma desconhecido. O nome do arquivo,
    excluindo-se a extensão, deve ter um código de idioma válido. Os cabeçalhos
    devem ajudar a encontrar a qual idioma se refere, de outro modo é inútil
    incluir o arquivo num pacote se ninguém vai utilizá-lo.
  </dd>
  <dt><a name="missingfile">missing-file-in-POTFILES.in</a></dt>
  <dd>
    O debian/po/POTFILES.in se refere aos arquivos de modelo inexistentes. Este
    erro é geralmente encontrado após arquivos de modelo serem renomeados ou
    removidos. É responsabilidade dos(as) mantenedores(as) que corrijam este
    arquivo, atualizem os arquivos PO e demandem por traduções atualizadas.
    Os(As) tradutores(as) <strong>não</strong> devem trabalhar nesses pacotes,
    já que, geralmente, o arquivo templates.pot está muito desatualizado.
  </dd>
  <dt><a name="template">not-up-to-date-templates.pot</a></dt>
  <dd>
    O debian/po/templates.pot não foi sincronizado com os arquivos de modelos.
    Os(As) mantenedores(as) devem corrigir seus pacotes adicionando
    debconf-updatepo como alvo do clean no debian/rules. Os(As) tradutores(as)
    devem, em primeiro lugar, rodar o debconf-updatepo se estiverem trabalhando
    com o pacote-fonte. Os arquivos PO e POT no site web devem estar
    atualizados.
  </dd>
  <dt><a name="po">not-up-to-date-po-file</a></dt>
  <dd>
    Os arquivos listados não foram sincronizados com os modelos. Os(As)
    mantenedores(as) devem corrigir seus pacotes pela adição de debconf-updatepo
    como alvo do clean no debian/rules. Os(As) tradutores(as) devem, em primeiro
    lugar, rodar o debconf-updatepo se estiverem trabalhando com o pacote-fonte.
    Os arquivos PO e POT no site web devem estar atualizados.
  </dd>
</dl>
