#use wml::debian::translation-check translation="d5484a72e71b8da89fe25f18d5d9248fab4b56c6" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Denne opdatering indeholder opdateret CPU-microcode til nogle former for 
Intel-CPU'er, og leverer afhjælpning af sikkerhedssårbarheder, der kunne medføre 
rettighedsforøgelse i forbindelse med VT-d og forskellige sidekanalangreb.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 3.20210608.2~deb10u1.</p>

<p>Bemærk at der er to rapporterede regressioner; ved nogle CoffeeLake-CPU'er, 
kan denne opdatering medføre at iwlwifi holder op med at virke 
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/56)
og ved nogle Skylake R0/D0-CPUer på systemer, der anvender en meget forældet 
firmware/BIOS, vil systemet hænge ved boot:
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/31)</p>

<p>Hvis du er påvirket at disse problemer, kan du løse det ved at deaktivere 
indlæsning af microcode ved boot (som dokumenteret i README.Debian, der også er 
tilgængelig online på 
<a href="https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian">\
https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian)</a></p>

<p>Vi anbefaler at du opgraderer dine intel-microcode-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende intel-microcode, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4934.data"
