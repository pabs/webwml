#use wml::debian::template title="Debian 11 -- utgivelsesmerknader" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b" maintainer="Hans F. Nordhaug"

<if-stable-release release="stretch">
<p>Disse utgivelsesmerknadene for Debian 10, kodenavn buster, som ikke er utgitt enda,
er <strong>under utarbeidelse</strong>. Informasjonen presentert her kan være unøyaktig
og utdatert, og er mest sannsynlig ufullstendig.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Disse utgivelsesmerknadene for Debian 11, kodenavn bullseye,, som ikke er utgitt enda,
er <strong>under utarbeidelse</strong>. Informasjonen presentert her kan være unøyaktig
og utdatert, og er mest sannsynlig ufullstendig.</p>
</if-stable-release>

<p>For å finne ut hva som er nytt i Debian 11, les utgivelsesmerknadene for din arkitektur:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Utgivelsesmerknader'); :>
</ul>

<p>Utgivelsesmerknadene inneholder også informasjon for brukere som oppgraderer fra tidligere utgaver.</p>

<p>Hvis du har satt opp lokalisering i nettleseren din riktig, kan du bruke lenkene
ovenfor til å hente den rette HTML-versjonen automatisk &mdash; se <a href="$(HOME)/intro/cn">innholdsforhandling</a>.
Ellers kan du velge maskinarkitektur, språk og format fra tabellen nedenfor.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arkitektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Språk</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
