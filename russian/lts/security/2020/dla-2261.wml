#use wml::debian::translation-check translation="66bf90f8fc76700f0dc4c61f3c55a800798ab361" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности LTS</define-tag>
<define-tag moreinfo>

<p>Было обнаружено, что уязвимость в php5, серверном языке сценариев со
встроенной поддержкой HTML, может приводить к исчерпанию дискового пространства на
сервере. При использовании слишком длинных имён файлов или имён полей может сработать
ограничение памяти, что приводит к остановке загрузки без последующей
очистки.</p>

<p>Кроме того, поставляемая версия утилиты <q>file</q> уязвима к
<a href="https://security-tracker.debian.org/tracker/CVE-2019-18218">CVE-2019-18218</a>.
Поскольку эту уязвимость можно использовать в php5 также, как и в file, данная
проблема не имеет собственного идентификатора CVE, а лишь сообщение об ошибке. Эта ошибка
была исправлена в данном обновлении (ограничение числа элементов CDF_VECTOR для предотвращения
переполнения буфера (4-байтовая запись за пределы выделенного буфера памяти)).</p>


<p>В Debian 8 <q>Jessie</q> эта проблема была исправлена в версии
5.6.40+dfsg-0+deb8u12.</p>

<p>Рекомендуется обновить пакеты php5.</p>

<p>Дополнительную информацию о рекомендациях по безопасности Debian LTS,
о том, как применять эти обновления к вашей системе, а также ответы на часто
задаваемые вопросы можно найти по адресу: <a href="https://wiki.debian.org/LTS">\
https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2261.data"
