# Turkish messages for Debian Web pages (debian-webwml).
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as Debian Web pages.
#
# Murat Demirten <murat@debian.org>, 2002, 2003, 2004.
# Recai Oktaş <roktas@omu.edu.tr>, 2005.
# Onur Aslan <onur@onur.im>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-11-10 15:06+0100\n"
"PO-Revision-Date: 2013-02-11 21:34+0200\n"
"Last-Translator: Onur Aslan <onur@onur.im>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=1; plural=0;\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Eposta Listesi Üyeliği"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Eposta kullanarak nasıl üye olacağınıza ait bilgileri <a href=\"./#subunsub"
"\">mail listeleri</a> sayfasından edinebilirsiniz.  Liste üyeliklerden "
"çıkmak için ayrıca <a href=\"unsubscribe\">üyelikten çıkma formu</a> "
"mevcuttur."

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Not: Çoğu Debian eposta listesi açık forumdur. Gönderilen her eposta, açık "
"eposta listesi arşivlerinde yayınlanacak ve arama motorları tarafından "
"indekslenecektir. Debian eposta listelerine sadece herkese vermekten "
"kaçınmadığınız bir eposta ile üye olmalısınız."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Lütfen üye olmayı istediğiniz listeyi seçin (üye olacağınız listeler "
"sınırlıdır, eğer isteğiniz tamamlanmadıysa lütfen <a href=\"./#subunsub"
"\">başka bir yol</a> deneyin):"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Açıklama verilmemiş"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Kısıtlanmış:"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Sadece üyeler eposta gönderebilir."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr ""
"Bu listeye sadece bir Debian geliştiricisi tarafından imzalanmış iletiler "
"kabul edilecektir."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Üyelik:"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "salt-okunabilir, derlenmiş sürüm."

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "Eposta adresiniz:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "Üye ol"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Temizle"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Lütfen <a href=\"./#ads\">Debian eposta listeleri reklam politikasına</a> "
"saygılı olunuz."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Eposta Liste Üyeliğinden Çıkma"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Eposta göndererek nasıl liste üyeliğinden çıkabileceğinize dair bilgileri <a "
"href=\"./#subunsub\">mail listeleri</a> sayfasından edinebilirsiniz.  Başka "
"listelere de üye olmak isterseniz ayrıca bir <a href=\"subscribe\">üyelik "
"formu</a> mevcuttur."

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Lütfen üye olmaktan çıkmak istediğiniz listeyi seçin (üyelikten çıkacağınız "
"listeler sınırlıdır, eğer isteğiniz tamamlanmadıysa lütfen <a href=\"./"
"#subunsub\">başka bir yol</a> deneyin):"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Üyelikten Çık"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "açık"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "kapalı"
