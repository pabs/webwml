#use wml::debian::template title="Debian GNU/Linux 2.2 -- Notities bij de release" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/potato/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="0a158377b74b807d40736c5de7ac54c071d55714"

<p>Om te weten wat nieuw is in Debian 2.2, raadpleegt u best de notities bij de
release voor uw architectuur:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Release Notes'); :>
</ul>

<p>De notities bij de release voor architecturen die reeds eerder uitgebracht
werden, bevatten ook instructies voor gebruikers die opwaarderen vanaf een
eerdere release.</p>

<p>Als u in uw browser de lokalisatie passend heeft ingesteld,
dan kunt u bovenstaande link gebruiken om automatisch de juiste HTML-versie te
verkrijgen -- zie <a href="$(HOME)/intro/cn">content negotiation</a>
(onderhandelen over de inhoud).
Selecteer anders exact de gewenste architectuur, taal en formaat in de
onderstaande tabel.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architectuur</strong></th>
  <th align="left"><strong>Formaat</strong></th>
  <th align="left"><strong>Taal</strong></th>
</tr>
<: &permute_as_matrix('release-notes', keys %langsrelnotes); :>
</table>
</div>

<p>Er is een <a href="http://master.debian.org/~lapeyre/reports/">gedetailleerd
rapport</a> beschikbaar waarin de pakketten beschreven worden die tijdens de laatste twee releases veranderd zijn in de architecturen
<:= $arches{'i386'} :>, <:= $arches{'alpha'} :>,
<:= $arches{'sparc'} :> en <:= $arches{'m68k'} :>.</p>
