#use wml::debian::template title="Langetermijnondersteuning - Beveiligingsinformatie" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="51b1f2756d728aa0beb3ba7e5f67a15ab3b8db98"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Uw Debian LTS-systeem veilig houden</toc-add-entry>

<p>Om de laatste Debian LTS-beveiligingsadviezen te ontvangen, moet u zich
abonneren op de mailinglijst
<a href="https://lists.debian.org/debian-lts-announce/">debian-lts-announce</a>.</p>

<p>Voor meer informatie over beveiligingskwesties in Debian, verwijzen wij u
naar de <a href="../../security">Beveiligingsinformatie van Debian</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Recente adviezen</toc-add-entry>

<p>Deze webpagina's bevatten een beknopt archief van beveiligingsadviezen die op
de lijst <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a> zijn geplaatst.</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>De recentste Debian LTS-beveiligingsadviezen zijn ook beschikbaar in
<a href="dla">RDF-formaat</a>. We bieden ook een
<a href="dla-long">tweede bestand</a> aan dat tevens de eerste paragraaf bevat
van het bijbehorende advies, zodat u daarin kunt zien waar het advies over
gaat.</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Ook oudere beveiligingsadviezen zijn beschikbaar:
<:= get_past_sec_list(); :>

<p>Debian-distributies zijn niet voor alle beveiligingsproblemen kwetsbaar. Het
<a href="https://security-tracker.debian.org/">Beveiligingsvolgsysteem van
Debian</a> verzamelt alle informatie over de kwetsbaarheidsstatus van
Debian-pakketten en kan worden doorzocht op CVE-naam of op pakket.</p>


<toc-add-entry name="contact">Contactinformatie</toc-add-entry>

<p>Raadpleeg eerst de <a href="https://wiki.debian.org/LTS/FAQ">Debian LTS-FAQ</a> vooraleer u ons contacteert. Uw vraag is daar wellicht al beantwoord!</p>

<p>Ook de <a href="https://wiki.debian.org/LTS/FAQ">contactinformatie is te
vinden in de FAQ</a>.</p>
