#use wml::debian::template title="De gedragscode van Debian interpreteren" BARETITLE=true
#use wml::debian::translation-check translation="8ec27f98071e8313b1a32572c961a2417fc0f36f"

{#meta#:
<meta name="keywords" content="code of conduct, coc">
:#meta#}

<h2>Enkele algemene opmerkingen</h2>

<p>Het doel van dit document is om enige uitleg en voorbeelden te geven over
hoe de <a href="$(HOME)/code_of_conduct">Gedragscode</a> (Code of Conduct - CoC)
wordt geïnterpreteerd binnen het Debian-project. Als u vragen heeft, neem dan
contact op met het Community Team (community@debian.org). Als u bezorgd bent
dat iets dat u overweegt te doen de gedragscode zou kunnen schenden, neem ook
in dat geval contact op met het Community Team.</p>

<p>Het doel van de gedragscode is om een gemeenschapsruimte te creëren waar
mensen zich op hun gemak voelen. Dit helpt ons een collectief van medewerkers
in stand te houden die enthousiast zijn om deel te nemen aan Debian en ons
helpen onze doelen te bereiken, namelijk het creëren en onderhouden van Debian.
De Debian-gemeenschap is tegelijkertijd zowel een groep vrienden die aan een
project werken als een groep collega's die hun werk doen. Debian is net zo goed
een grote sociale groep als een technisch project.</p>

<p>Het doel van de gedragscode en het doel van de inspanningen om de
gedragscode te handhaven, is om mensen te helpen zich af te stemmen op de
gedeelde waarden die Debian als gemeenschap heeft aangenomen via het proces van
Algemene Resoluties. U moet de gedragscode volgen en respecteren om deel te
kunnen nemen aan Debian. U hoeft niet perfect te zijn -- iedereen maakt fouten
of heeft wel eens een slechte dag -- het doel van de handhaving van de
gedragscode is om mensen te helpen het beter te doen. Dat is alles wat er van u
wordt gevraagd: doe uw best om uw vrienden en collega's met respect te
behandelen. De gedragscode heeft betrekking op alle activiteiten binnen
Debian en diegene die u uitvoert als vertegenwoordiger van het Debian-project.
Strafmaatregelen, zoals (tijdelijke of permanente) uitsluiting of verlies van
status, kunnen ook voorkomen als uw niet-Debian activiteiten invloed hebben op
het Project of binnen Debian een onveilige of schadelijke omgeving creëren.</p>

<p>Dit is een <strong>levend document</strong>. Het zal mettertijd veranderen
naarmate wat als normaal en ideaal wordt beschouwd zowel binnen als buiten
Debian evolueert. De Debian gedragscode werd voor het eerst goedgekeurd in
2014. Ze is sindsdien niet veranderd, hoewel de verwachtingen en goede
praktijken in de vrije en opensourcesoftwaregemeenschap en in de technologie
in het algemeen, wel zijn veranderd.</p>

<h2>1. Wees respectvol</h2>

<p><strong>In een project van de omvang van Debian zullen er onvermijdelijk
mensen zijn met wie u het misschien niet eens bent, of waarmee u moeilijk kunt
samenwerken. Accepteer dat, maar blijf toch respectvol. Het oneens zijn met
iemands acties of meningen is geen excuus voor slecht gedrag of persoonlijke
aanvallen. Een gemeenschap waarin mensen zich bedreigd voelen, is geen gezonde
gemeenschap.</strong></p>

<p>Ieder lid van de gemeenschap en de wereld daarbuiten verdient respect.
Respect is niet iets dat verdiend moet worden in Debian, het is iets waarop elk
lid van de gemeenschap recht heeft, ongeacht leeftijd, geslacht,
lichaamsgrootte, opleiding, etniciteit, of andere factoren.</p>

<p>Debian is een open gemeenschap. Alle culturen en overtuigingen zijn welkom
en worden erkend, zolang ze anderen niet schaden. Uw eigen verwachtingen of uw
eigen culturele achtergrond zijn geen excuus om de gedragscode te schenden of
respectloos te zijn tegenover een andere persoon binnen de Debian-gemeenschap
of in het kader van uw rol als lid van de Debian-gemeenschap. Debian heeft zijn
eigen cultuur. Wanneer u werkt met medewerkers of gebruikers van Debian dient u
zich aan de normen van Debian te houden en Debian op een positieve manier te
vertegenwoordigen.</p>

<p>Mensen in Debian hebben verschillende culturele achtergronden, hebben
verschillende ervaringen, en zijn misschien niet vloeiend of comfortabel in de
taal waarin een bepaalde discussie gevoerd wordt. Dit betekent dat het
belangrijk is om uit te gaan van de beste bedoelingen (zie hieronder) en begrip
te hebben voor verschillen in communicatiestijl. Dit betekent echter niet dat
het aanvaardbaar is om opzettelijk ongepast te communiceren of uw
communicatiestijl niet aan te passen om aan de normen van de gemeenschap te
voldoen, zodra dit ter sprake is gebracht.</p>

<h3>Voorbeelden van respectloos gedrag</h3>

<p>Hieronder volgt een <strong>niet-exhaustieve</strong> lijst van voorbeelden
van respectloos gedrag:</p>

<ul>
  <li>Het gebruik van vulgaire of krenkende taal tegen een persoon of
      zijn/haar werk</li>
  <li>Iemand beledigen wegens leeftijd, handicap, geslacht, seksualiteit,
      lichaamsomvang, religie, nationaliteit, ras, etniciteit, kaste, stam,
      opleiding, type bijdrage, of status binnen Debian en/of de vrije en
      opensourcesoftware, één van de bovenstaande als belediging gebruiken, of
      denigrerende uitspraken doen over een groep</li>
  <li>Opzettelijk een verkeerd voornaamwoord of een verkeerde naam gebruiken
      voor een persoon (bijv. <q>deadnaming</q>, d.w.z. iemand moedwillig bij
      de oude naam noemen)</li>
  <li>Agressief of herhaaldelijk contact zoeken met iemand nadat gevraagd is
      dit te stoppen</li>
  <li>Geen pogingen te goeder trouw doen om overeenstemming te bereiken met
      mensen of gedrag te veranderen dat in strijd is met de waarden van
      Debian</li>
</ul>

<h2>2. Ga uit van goede trouw</h2>

<p><strong>Medewerkers van Debian hebben vele manieren om ons gemeenschappelijk
doel van een vrij besturingssysteem te bereiken: de manier waarop iemand anders
iets doet kan verschillen van uw manier van werken. Ga ervan uit dat andere
mensen in een geest van samenwerking ernaar streven om dit doel te bereiken.
Merk op dat veel van onze medewerkers niet het Engels als moedertaal hebben en
dat zij een andere culturele achtergrond kunnen hebben.</strong></p>

<p><strong>Debian is een wereldwijd project.</strong> Debian omvat mensen met
veel verschillende achtergronden, ervaringen, communicatiestijlen en culturele
normen. Als zodanig is het bijzonder belangrijk om uit te gaan van goede trouw.
Dit betekent redelijkerwijs aannemen dat de persoon met wie u praat u niet
probeert te kwetsen of te beledigen.</p>

<p>Om van goede trouw uit te gaan, moeten we ook te goeder trouw handelen. Dit
betekent ook dat u moet aannemen dat iemand zijn best doet, en dat u hem/haar
niet mag kwetsen of beledigen. Het opzettelijk kwetsen van iemand binnen Debian
is niet acceptabel.</p>

<p>Uitgaan van goede trouw omvat communicatie, gedrag en inbreng. Dit betekent
dat ervan wordt uitgegaan dat iedereen die een bijdrage levert, ongeacht zijn
bijdrage, de inspanning levert waartoe hij/zij in staat is en dit doet op een
integere wijze.</p>

<h3>Voorbeelden van gedrag dat niet te goeder trouw is</h3>

<p>Nogmaals, het volgende is <strong>geen exhaustieve</strong> lijst:</p>

<ul>
  <li>Trollen</li>
  <li>Aannemen dat iemand aan het trollen is</li>
  <li>Zinnen gebruiken zoals <q>Ik weet dat u niet stom bent</q> of <q>Dat
      kunt u niet opzettelijk gedaan hebben, dus moet u wel dom zijn</q></li>
  <li>Iemands bijdragen beledigen</li>
  <li>Antagonisme opwekken bij anderen - <q>mensen op de kast jagen</q> om een
      effect teweeg te brengen</li>
</ul>

<h2>3. Wees collaboratief</h2>

<p><strong>Debian is een groot en complex project; men kan altijd bijleren
binnen Debian. Het is goed om hulp te vragen wanneer u die nodig heeft. Ook een
aanbod om hulp te bieden moet worden gezien in de context van ons gezamenlijke
doel om Debian beter te maken.</strong></p>

<p><strong>Als u iets maakt ten bate van het project, wees dan bereid om
anderen uit te leggen hoe het werkt, zodat zij op uw werk kunnen voortbouwen om
het nog beter te maken.</strong></p>

<p>Onze bijdragen helpen andere personen die bijdragen leveren, het project en
onze gebruikers. We werken in het openbaar onder het ethos dat iedereen die wil
bijdragen dat ook zou moeten kunnen, binnen redelijke grenzen. Iedereen in
Debian heeft een andere achtergrond en andere vaardigheden. Dit betekent dat u
positief en constructief moet zijn en, waar mogelijk, hulp, advies of
mentorschap zou moeten bieden. We hechten waarde aan consensus, hoewel er soms
democratische beslissingen zullen worden genomen of de richting zal worden
bepaald door die mensen die bereid en in staat zijn om een activiteit te
ondernemen.</p>

<p>Verschillende teams gebruiken verschillende hulpmiddelen en hebben
verschillende standaarden op het gebied van samenwerking. Dit kunnen
bijvoorbeeld wekelijkse synchrone vergaderingen zijn, gemeenschappelijke
notulen, of processen voor de evaluatie van code. Het is niet omdat dingen ooit
op een bepaalde manier zijn gedaan dat dit betekent dat dit de beste of enige
manier is om de dingen te doen, en teams zouden open moeten staan voor het
bespreken van nieuwe samenwerkingsmethoden.</p>

<p>Onderdeel van samenwerken is ook te goeder trouw aannemen (zie boven) dat
ook de anderen tot samenwerken geneigd zijn, in plaats van te veronderstellen
dat anderen <q>erop uit zijn om u te pakken</q> of u te negeren.</p>

<p>Een goede samenwerking is waardevoller dan technische vaardigheden. Het feit
dat iemand goede technische bijdragen levert, maakt het nog niet acceptabel om
een schadelijk lid van de gemeenschap te zijn.</p>

<h3>Voorbeelden van slechte samenwerking</h3>

<ul>
  <li>Weigeren om de standaarden van een team op het gebied van samenwerking
      aan te nemen</li>
  <li>Andere personen die bijdragen leveren, of collega's beledigen</li>
  <li>Weigeren om met anderen samen te werken, tenzij dit een bedreiging vormt
      voor uw veiligheid of uw welzijn</li>
</ul>

<h2>4. Probeer bondig te zijn</h2>

<p><strong>Bedenk dat wat u één keer schrijft, door honderden mensen zal worden
gelezen. Een korte e-mail is het meest efficiënt voor een goed begrip
van de conversatie. Als een lange uitleg nodig is, overweeg dan om een
samenvatting toe te voegen.</strong></p>

<p><strong>Probeer nieuwe argumenten in de conversatie in te brengen, zodat
elke e-mail iets unieks toevoegt aan de discussie, waarbij u in gedachten houdt
dat de rest van de discussie nog steeds de andere berichten bevat met de
argumenten die al zijn aangevoerd.</strong></p>

<p><strong>Probeer bij het onderwerp te blijven, vooral in discussies die al
vrij groot zijn.</strong></p>

<p>Bepaalde onderwerpen zijn niet geschikt voor Debian, waaronder een aantal
controversiële onderwerpen van politieke of religieuze aard. Debian is een
omgeving van collega's net zo goed als het een omgeving van vrienden is.
Openbare collectieve uitwisselingen zouden respectvol moeten gebeuren, over het
onderwerp moeten gaan en professioneel moeten zijn. Het gebruik van beknopte,
toegankelijke taal is belangrijk, vooral omdat veel Debian-medewerkers niet het
Engels als moedertaal hebben en veel van de communicatie binnen het project in
het Engels plaatsvindt. Het is belangrijk om duidelijk en expliciet te zijn, en
waar mogelijk taaleigen uitdrukkingen uit te leggen of te vermijden.
(Opmerking: Het gebruik van taaleigen uitdrukkingen, bijvoorbeeld, is geen
schending van de gedragscode. Het vermijden ervan is gewoon een goede gewoonte
in het algemeen voor de duidelijkheid).</p>

<p>Het is niet altijd gemakkelijk om betekenis en toon over te brengen via
tekst of over culturen heen. Openheid van geest, uitgaan van goede bedoelingen
en inspanningen doen zijn de belangrijkste dingen in een conversatie.</p>

<h2>5. Wees open</h2>

<p><strong>De meeste communicatiemethoden die binnen Debian worden gebruikt,
maken openbare en privécommunicatie mogelijk. Volgens paragraaf drie van het
sociaal contract moet u bij voorkeur openbare communicatiemethoden gebruiken
voor berichten die betrekking hebben op Debian, tenzij u iets gevoelig post.</strong></p>

<p><strong>Dit geldt ook voor berichten met vragen om hulp of
Debian-gerelateerde ondersteuning; niet alleen is de kans veel groter dat een
openbaar verzoek om ondersteuning resulteert in een antwoord op uw vraag, het
zorgt er ook voor dat eventuele onopzettelijke fouten die worden gemaakt door
mensen die uw vraag beantwoorden, gemakkelijker kunnen worden opgespoord en
gecorrigeerd.</strong></p>

<p>Het is belangrijk om zoveel mogelijk communicatie openbaar te houden. Op
veel Debian-mailinglijsten kan door iedereen worden ingetekend of ze hebben
openbaar toegankelijke archieven. Archieven kunnen worden vastgelegd en
opgeslagen door niet-Debian bronnen (bijv. het Internetarchief). Men moet ervan
uitgaan dat wat op een mailinglijst van Debian is gezegd, <q>permanent</q> is.
Veel mensen houden ook logboeken bij van IRC-gesprekken.</p>

<h3>Privacy</h3>

<p>Privégesprekken binnen de context van het project worden nog steeds geacht
onder de gedragscode te vallen. Het wordt aangemoedigd om te melden dat iets
wat in een privégesprek wordt gezegd, ongepast of onveilig is (zie hierboven
voor voorbeelden).</p>

<p>Tegelijkertijd is het belangrijk om privégesprekken te respecteren en mogen
deze niet gedeeld worden, behalve als het om veiligheidskwesties gaat. Bepaalde
plaatsen, zoals de mailinglijst debian-private, vallen onder deze categorie.</p>

<h2>6. In geval van problemen</h2>

<p><strong>Hoewel deze gedragscode door de deelnemers moet worden nageleefd,
erkennen wij dat mensen soms een slechte dag kunnen hebben, of niet op de
hoogte zijn van sommige richtlijnen in deze gedragscode. Wanneer dat gebeurt,
kunt u hen antwoorden en hen op deze gedragscode wijzen. Dergelijke berichten
kunnen openbaar of privé zijn, wat het meest gepast is. Maar ongeacht of het
bericht openbaar is of niet, het moet nog steeds voldoen aan de relevante delen
van deze gedragscode; met name mag het niet beledigend of respectloos zijn. Ga
uit van goede trouw; het is waarschijnlijker dat deelnemers zich niet bewust
zijn van hun slechte gedrag dan dat zij opzettelijk proberen de kwaliteit van
de discussie onderuit te halen.</strong></p>

<p><strong>Ernstige of hardnekkige overtreders zullen tijdelijk of permanent
worden uitgesloten van communicatie via de systemen van Debian. Klachten moeten
(privé) worden ingediend bij de beheerders van het betreffende
communicatieforum van Debian. Raadpleeg de pagina over de organisatiestructuur
van Debian om de contactgegevens van deze beheerders te vinden.</strong></p>

<p>Het doel van de gedragscode is om mensen richtlijnen te geven over hoe ze
Debian als een gastvrije gemeenschap kunnen handhaven. Mensen zouden zich
welkom moeten voelen om deel te nemen zoals ze zijn en niet zoals anderen
verwachten dat ze zijn. De Debian gedragscode schetst dingen die mensen zouden
moeten doen, in plaats van manieren waarop ze zich niet zouden moeten gedragen.
Dit document geeft inzicht in hoe het Community Team de gedragscode
interpreteert. Verschillende leden van de Debian-gemeenschap kunnen dit
document anders interpreteren. Wat het belangrijkste is, is dat mensen zich
comfortabel, veilig en welkom voelen binnen Debian. Ongeacht of het specifiek
in dit document wordt vermeld of niet, als iemand zich niet comfortabel, veilig
en/of welkom voelt, moet hij/zij contact opnemen met het <a
href="https://wiki.debian.org/Teams/Community">Community Team</a>.</p>

<p>Als iemand bezorgd is dat hij/zij iets ongepast heeft gedaan of erover denkt
iets te doen waarvan hij/zij denkt dat het ongepast is, wordt hij/zij ook
aangemoedigd contact op te nemen met het Community Team.</p>

<p>Omdat Debian zo groot is, kan en zal het Community Team niet proactief alle
communicatie in de gaten houden, hoewel leden het team soms in het voorbijkomen
te zien krijgen. Als zodanig is het belangrijk voor de Debian-gemeenschap om
samen te werken met het Community Team.</p>

<h2>Niet naleven van de gedragscode</h2>

<p>Van niemand wordt verwacht dat die altijd perfect is. Een fout maken is niet
het einde van de wereld, maar het kan er wel toe leiden dat iemand contact
opneemt om te vragen om verbetering. Binnen Debian wordt verwacht dat men te
goeder trouw inspanningen levert om het goed en beter te doen. Herhaaldelijke
schendingen van de gedragscode kunnen resulteren in represailles of beperkingen
in de interactie met de gemeenschap, inclusief, maar niet beperkt tot:</p>

<ul>
  <li>officiële waarschuwingen;</li>
  <li>tijdelijke of permanente uitsluiting van e-maillijsten, IRC-kanalen of
      andere communicatiemiddelen;</li>
  <li>tijdelijke of permanente intrekking van rechten en voorrechten; of</li>
  <li>tijdelijke of permanente degradatie van status binnen het
      Debian-project.</li>
</ul>
